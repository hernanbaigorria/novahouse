$(document).on('submit', '.frm-suscribe', function(e)
{ 
    var form = $(this);
    form.find('.lbl-suscripto').addClass('hidden');
    form.find('.lbl-error-suscripto').addClass('hidden');
    $.ajax({
        type: "POST",
        url: '/suscriptores/ajax/registrarse',
        data: form.serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if (data.cod == 1) 
            {   
                window.location = "/gracias";
                form[0].reset();
                form.find('.lbl-suscripto').removeClass('hidden');
            }
            if (data.cod == 0) 
            {
                form.find('.lbl-error-suscripto').removeClass('hidden');
            };
        },
        dataType: "json"
    });
    e.preventDefault();
});