<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_suscriptores_suscriptores');
        $this->lang->load('general_lang', $this->session->userdata('user_language'));
        $this->configs =  $this->configurations->get_configurations('module:suscriptores', 0, get_web_lang());
    }

    // Llamados de gestion interna. No olvidar controlar la sesion y los permisos.

        public function del_suscriptor()
        {
            $this->control_library->session_check();

            $id_suscriptor = $this->input->post('id_suscriptor');

            // Realizamos el update.
            $result = $this->mod_suscriptores_suscriptores->del_suscriptor($id_suscriptor);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/suscriptores?action=list';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Suscriptor eliminado correctamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al eliminar el suscriptores.', '0', '1');
        }

    // LLamados publicos.

        public function registrarse()
        {
            $email = $this->input->post('email');
            $telefono = $this->input->post('telefono');

            $data['nombre'] = $this->input->post('nombre');
            $data['telefono'] = $this->input->post('telefono');
            $data['email'] = $this->input->post('email');
            $data['metros'] = $this->input->post('metros');
            $data['consulta'] = $this->input->post('consulta');

            $envio_sub = $this->configurations->get_configurations('module:suscriptores', 0, get_web_lang());

            $subject = "Nova House - Nuevo contacto";

            $content = $this->load->view('emails/externos/suscriptor_enviado', $data, TRUE);

            $this->load->model('notifications');

            $sub_enviado = $this->notifications->send($envio_sub['email_para_avisos'], $subject, $content, $on_debug = 'EM');
            

            $this->load->helper('email');
            if (!valid_email($email) OR strlen($email) <= 6)
                ajax_response('danger', 'Ocurrió un problema registrando tu email.', '0', '1');

            $id_suscriptor = $this->mod_suscriptores_suscriptores->already_registered($email);

            if (!$id_suscriptor) 
                $id_suscriptor = $this->mod_suscriptores_suscriptores->new_suscriptor($email, $telefono);
            
            if ($id_suscriptor !== FALSE)
                ajax_response('success', 'Gracias por suscribirte.', '1', '1');

            ajax_response('success', 'Ocurrió un problema. Por favor intenta mas tarde.', '0', '1');
        }
        
}