<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suscriptores extends MX_Controller {

	// System 

		private $current_ejemplar 	= FALSE;

	    function __construct() {
	        parent::__construct();
	        $this->load->model('mod_suscriptores_suscriptores');
	        $this->configs =  $this->configurations->get_configurations('module:suscriptores', 0, get_web_lang());
	    }

		public function _remap( $method )
		{
			$segs 	= $this->uri->segment_array();
	        $params	= array_slice($segs, array_search($method, $segs));
	        
	        // Bloqueamos determinados metodos usadospor el gestor.
		    if (in_array($method, array('module_managment'))) 
		    	show_404();
		    else 
		    {
		    	// Si llegamos aca, puede que estemos intentando acceder a un metodo publico. Veamos si exise.
		    	if (method_exists($this, $method)) {
		    		$this->$method(implode(',', $params));
		    	}
		    	// Es un ejemplar?.
		    	elseif ($this->load_ejemplar($method)) {
		    		$this->render_ejemplar();
		    	}
		    	// Error 404..
		    	else {
		    		show_404();
		    	}
		    }
		}

	// Public Access

	    // Lista todos los suscriptores del criadero
		public function index()
		{
			show_404();
		}

	// API

		public function get_suscriptores($cond = FALSE, $value = FALSE, $page = 0, $max = 10, $order = FALSE)
		{
			$this->control_library->session_check();
			return $this->mod_suscriptores_suscriptores->get_suscriptores($cond, $value, $page, $max, $order);
		}


	// Administracion del modulo.

		public function module_managment()
		{
			$this->control_library->session_check();
			
			// Levantamos variables GET
			$id_suscriptor = $this->input->get('id');
			$action 	= $this->input->get('action');
			$page 	 	= (int)$this->input->get('pagina');
			$busqueda	= $this->input->get('busqueda');
			$categoria	= $this->input->get('categoria');
			$page 	 	= ($page > 0) ? $page - 1 : 0;

			switch ($action) {
				case 'edit':
					$data['suscriptor'] = $this->mod_suscriptores_suscriptores->get_suscriptor($id_suscriptor);
					$this->load->view('manager/suscriber_editor', $data);
					break;
				case 'new':
					$data['colores'] 	= $this->mod_suscriptores_colores->get_colores(FALSE, FALSE, 0, 500);
					$data['variedades'] = $this->mod_suscriptores_variedades->get_variedades(FALSE, FALSE, 0, 500);
					$this->load->view('manager/suscriber_creator', $data);
					break;
				default:
					$data['suscriptores'] = $this->mod_suscriptores_suscriptores->get_suscriptores(FALSE, FALSE, $page, 15, array('sus_fecha' => 'DESC'), $busqueda);
					
					$data['pagination_current']		= $page;
					$data['pagination_total_items']	= @$data['suscriptores'][0]['total_results'];
					
					$data['pagination_per_page']	= 15;
					$this->load->view('manager/main', $data);
					break;
			}
		}

		public function exportar()
		{
			$data['suscriptores'] = $this->mod_suscriptores_suscriptores->get_suscriptores(FALSE, FALSE, 0, 10000, array('sus_fecha' => 'DESC'));
			$this->load->view('manager/export', $data);
		}


		public function config()
		{
			$test = $this->configurations->get_configurations('module:suscriptores', 0, get_web_lang());
			$this->load->view('manager/configuracion', $test);
		}

		public function opciones()
		{
			$this->control_library->session_check();
			
			// El form es generado por el gestor directamente. Las opciones de config estan en config_schema de este modulo.
			$this->load->view('manager/main_options', FALSE);
		}
}
