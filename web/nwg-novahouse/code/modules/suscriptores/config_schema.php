<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// Titulo de la pagina
	$ADDED_CFG['email_para_avisos']['default']		= 'chirino.carlos@gmail.com';
	$ADDED_CFG['email_para_avisos']['multilang'] 	= FALSE;
	$ADDED_CFG['email_para_avisos']['permissions'] 	= FALSE;
	$ADDED_CFG['email_para_avisos']['type'] 		= 'text';