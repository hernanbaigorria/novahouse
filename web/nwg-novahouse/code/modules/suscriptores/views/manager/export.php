<div class="tabbable tabbable-custom tabbable-noborder ">
    
    <ul class="nav nav-tabs">
        <li class="">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores">
                Suscriptores
            </a>
        </li>
        <li class="active">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores/exportar">
                Exportar
            </a>
        </li>
        
        <li class="">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores/config">
                Configuracion
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content innerAll">
                    <textarea class="form-control" rows="20"><?php foreach ($suscriptores as $_ksuscriptor => $_suscriptor): ?><?php echo $_suscriptor['sus_email'].PHP_EOL ?><?php endforeach ?></textarea>
                </div>
            </div>
        </div>
    </div>
    
</div>