<div class="tabbable tabbable-custom tabbable-noborder ">
    
    <ul class="nav nav-tabs">
        <li class="">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores">
                Suscriptores
            </a>
        </li>
        <li class="">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores/exportar">
                Exportar
            </a>
        </li>

        <li class="active">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores/config">
                Configuracion
            </a>
        </li>


    </ul>

    <div class="tab-content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <?php echo generate_multi_configuration_form('module:suscriptores') ?>
            </div>
        </div>
    </div>    
    
</div>