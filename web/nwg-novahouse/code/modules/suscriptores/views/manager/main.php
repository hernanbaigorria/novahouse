<div class="tabbable tabbable-custom tabbable-noborder ">
    
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores">
                Suscriptores
            </a>
        </li>
        <li class="">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores/exportar">
                Exportar
            </a>
        </li>

        <li class="">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/suscriptores/config">
                Configuracion
            </a>
        </li>

    </ul>
    
    <div class="tab-content">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <form action="<?php echo generate_get_string(); ?>">
                    <div id="sample_1_filter" class="dataTables_filter innerLR innerT">
                        <label>
                            Buscar: &nbsp;
                            <input type="search" class="form-control input-sm input-small input-inline" name="busqueda" placeholder="Buscar ..." value="<?php echo $this->input->get('busqueda') ?>">
                        </label>
                        <button type="submit" class="hidden"></button>
                        <?php if (!empty($this->input->get('busqueda')) OR !empty($this->input->get('filtro')) OR !empty($this->input->get('categoria'))): ?>
                            <a class="" href="<?php echo generate_get_string('action', 'list', array('busqueda', 'filtro', 'categoria')) ?>"><i class="fa fa-times"></i> Limpiar búsqueda y filtros</a>
                        <?php endif ?>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="tab-content innerAll">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover table-vertical-align table-condensed">
                            <thead>
                                <tr>
                                    <th class="" style="width: 130px;"><small> Fecha </small></th>
                                    <th class=""><small> Email </small></th>
                                    <th class=""><small> Telefono </small></th>
                                    <th class=" text-right" style="width: 82px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($suscriptores as $_ksuscriptor => $_suscriptor): ?>
                                <tr>
                                    <td>
                                        <?php echo date_to_view($_suscriptor['sus_fecha']) ?>
                                    </td>
                                    <td>
                                        <?php echo $_suscriptor['sus_email'] ?> <br>
                                        <a class="small" href="mailto:<?php echo $_suscriptor['sus_email'] ?>" target="_blank">Escribir a <?php echo $_suscriptor['sus_email'] ?></a>
                                    </td>
                                    <td>
                                        <?php echo $_suscriptor['sus_telefono'] ?>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-xs btn-group-solid">
                                            <a class="btn red btn-xs margin-none btn-remove-suscriptor" href="javascript:;" data-id_suscriptor="<?php echo @$_suscriptor['id_suscriptor'] ?>" data-confirmation="¿Estas seguro?">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                    <?php echo generate_pagination($pagination_current, $pagination_total_items, $pagination_per_page); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).on('click', '.btn-remove-suscriptor', function(e)
{ 
    var params = $(this).data();

    bootbox.confirm(params.confirmation, function(result) 
    {
        if (result == true)
        {
            send_button('/suscriptores/Ajax/del_suscriptor', params, function(data)
            {
                if (data.cod == 1) 
                {

                };
            });
        }
    });

    e.preventDefault();
});
</script>