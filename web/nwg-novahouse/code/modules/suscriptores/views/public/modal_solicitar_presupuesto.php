<div class="modal fade" id="modal-solicitar-presupuesto" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id="frm-solicitar-presupuesto">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="font-size: 15px;">Nos alegra mucho tu interés en <?php echo @$ej_name ?></h4>
                </div>
                <div class="modal-body">
                    <p>Necesitamos algunos datos para confeccionarte un presupuesto.</p>
                    <h6>Datos de contacto</h6>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Tus datos
                            </label>  
                            <div class="col-md-4">
                                <input required id="nombre" name="nombre" type="text" placeholder="Nombre" class="form-control input-md" value="">
                            </div>
                            <div class="col-md-6">
                                <input required id="email" name="email" type="email" placeholder="Email" class="form-control input-md" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                Teléfono
                            </label>  
                            <div class="col-md-4">
                                <input required id="cod_area" name="cod_area" type="text" placeholder="Cod. Area " class="form-control input-md">
                            </div>                            
                            <div class="col-md-6">
                                <input required id="telefono" name="telefono" type="text" placeholder="Número" class="form-control input-md">
                            </div>
                        </div>                        
                    </div>

                    <h6>¿Alguna pregunta? Contanos un poco de vos ...</h6>
                    <div class="form-group">
                        <textarea name="comentario" placeholder="¿Porque quieres adoptar a <?php echo @$ej_name ?>? ¿Lo quieres como mascota familiar?" class="form-control input-md"/>
                    </div>

                    <h6>¿como preferirias pagar y recibir al cachorrito?</h6>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-4 control-label text-right">
                                Forma de entrega:
                            </label>  
                            <div class="col-md-8">
                                <select name="envio_medio" id="envio_medio" class="form-control">
                                    <option data-ask="¿Aproximadamente en que fecha?" value="retiro">Lo retirare personalmente en el criadero</option>
                                    <option data-ask="¿Aproximadamente en que fecha?" value="comisionista">Enviare a un comisionista a buscarlo</option>
                                    <option data-ask="¿En que ciudad vivis?" value="retiro">Quiero que me lo envien</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" id="envio_comentario">
                                ¿Aproximadamente en que fecha?
                            </label>  
                            <div class="col-md-8">
                                <input type="text" required name="envio_comentario" class="form-control input-md"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label text-right">
                                Medio de pago:
                            </label>  
                            <div class="col-md-8">
                                <select id="pago_medio" name="pago_medio" class="form-control">
                                    <option value="todos">Quiero saber de todas las opciones.</option>
                                    <option data-hid="1" value="efectivo">Efectivo</option>
                                    <option value="tarjeta">Tarjetas de crédito</option>
                                    <option data-hid="1" value="transferencia">Transferencia bancaria</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="pago_comentario">
                            <label class="col-md-4 control-label" >
                                ¿De cuales tarjetas dispones?
                            </label>  
                            <div class="col-md-8">
                                <input type="text" name="pago_comentario" class="form-control input-md"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input  type="hidden" name="ejemplar" value="<?php echo @$id_ejemplar ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button id="btn-solicitar-presupuesto" type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </form>
    </div>
</div>