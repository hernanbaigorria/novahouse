<div class="modal fade" id="modal-preferencias-suscripcion" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id="frm-actualizar-preferencias">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php if (!$is_new): ?>
                        <h4 class="modal-title" style="font-size: 15px;">Hola <?php echo ucfirst($suscriptor['sus_nombre']) ?></h4>
                    <?php else: ?>
                        <h4 class="modal-title" style="font-size: 15px;">¿Alguna preferencia?</h4>
                    <?php endif ?>
                </div>
                <div class="modal-body">
                    
                        <?php if (!$is_new): ?>
                            <p class="text-center">
                                El <?php echo date_to_view($suscriptor['sus_fecha']); ?> nos dejaste tus datos para que te contactemos cuando tengamos un cachorrito con las características aquí indicadas.
                            </p>
                            <h6>Actualizar Datos</h6>
                        <?php else: ?>
                            <p>Puedes indicarnos las características que te interesa que tenga tu perrito, y te contactaremos únicamente cuando tengamos uno como el que buscas.</p>
                            <h6>Tus Datos</h6>
                        <?php endif ?>

                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    ¿Como es tu nombre?
                                </label>  
                                <div class="col-md-10">
                                    <input required id="nombre" name="nombre" type="text" placeholder="Nombre" class="form-control input-md" value="<?php echo @$suscriptor['sus_nombre'] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Teléfono <br>
                                    <?php if ($is_new): ?>
                                    (opcional)
                                    <?php endif ?>
                                </label>  

                                <div class="col-md-3">
                                    <input <?php if (!$is_new) echo 'required' ?> id="cod_area" name="cod_area" type="text" placeholder="Cod. Area " class="form-control input-md">
                                </div>                            
                                <div class="col-md-7">
                                    <input <?php if (!$is_new) echo 'required' ?> id="telefono" name="telefono" type="text" placeholder="Número" class="form-control input-md">
                                </div>
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <h6>¿Que sexo buscas?</h6>
                                <div>
                                    <label class="checkbox-inline fixed">
                                        <input type="hidden" name="sexo[machito]" value="0">
                                        <input type="checkbox" name="sexo[machito]" value="1" <?php if ($intereses['sexo']['machito']) echo 'checked' ?>>
                                        Machito
                                    </label>
                                    <label class="checkbox-inline fixed">
                                        <input type="hidden" name="sexo[hembrita]" value="0">
                                        <input type="checkbox" name="sexo[hembrita]" value="1" <?php if ($intereses['sexo']['hembrita']) echo 'checked' ?>>
                                        Hembrita
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <h6>¿Que pelaje?</h6>
                                <div>
                                    <?php foreach ($variedades as $_kvariedad => $_variedad): ?>
                                    <?php if ($_variedad['id_variedad'] != '1'): ?>
                                    <label class="checkbox-inline fixed">
                                        <input type="hidden" name="variedad[<?php echo $_variedad['id_variedad'] ?>]" value="0">
                                        <input type="checkbox" name="variedad[<?php echo $_variedad['id_variedad'] ?>]" value="1" <?php if ($intereses['variedad'][$_variedad['id_variedad']]) echo 'checked' ?>>
                                        <?php echo $_variedad['variedad_name'] ?>
                                    </label>
                                    <?php endif ?>
                                    <?php endforeach ?>                                
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h6>¿Que color?</h6>
                                <div>
                                    <?php foreach ($colores as $_kcolor => $_color): ?>
                                    <?php if ($_color['id_color'] != '1'): ?>
                                    <label class="checkbox-inline fixed">
                                        <input type="hidden" name="color[<?php echo $_color['id_color'] ?>]" value="0">
                                        <input type="checkbox" name="color[<?php echo $_color['id_color'] ?>]" value="1" <?php if ($intereses['color'][$_color['id_color']]) echo 'checked' ?>>
                                        <?php echo $_color['color_name'] ?>
                                    </label>
                                    <?php endif ?>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input  type="hidden" name="suscriptor" value="<?php echo $id_suscriptor ?>">
                    <button type="button" class="btn btn-danger btn-baja-suscribe <?php if ($is_new) echo 'hidden' ?>" data-suscriber="<?php echo $suscriptor['id_suscriptor'] ?>" data-email="<?php echo $suscriptor['sus_email'] ?>">Darme de baja</button>
                    <button id="btn-cerrar-modal-preferencias" type="button" class="btn btn-default " data-dismiss="modal">Cerrar</button>
                    <button id="btn-guardar-modal-preferencias" type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>