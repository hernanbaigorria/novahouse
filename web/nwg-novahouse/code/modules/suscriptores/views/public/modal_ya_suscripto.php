<div class="modal fade" id="modal-preferencias-suscripcion" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hola <?php echo ucfirst($suscriptor['sus_nombre']) ?></h4>
            </div>
            <div class="modal-body">
                <p>El <?php echo date_to_view($suscriptor['sus_fecha']); ?> nos dejaste tus datos para que te contactemos cuando tengamos un cachorrito con las siguientes caracteristicas:</p>
                <ul>
                    <?php // SEXO ?>
                    <?php if ($intereses['sexo']['machito'] AND $intereses['sexo']['hembrita']): ?>
                        <li>Cualquier sexo.</li>
                    <?php else: ?>
                        <?php if ($intereses['sexo']['machito']): ?>
                        <li>Machitos.</li>
                        <?php else: ?>                            
                        <li>Hembritas.</li>
                        <?php endif ?>
                    <?php endif ?>

                    <?php // VARIEDAD ?>
                    <li>Variedades:  
                        <?php foreach ($variedades as $_kvarieadad => $_variedad): ?>
                            <?php if (!$intereses['variedad'][$_variedad['id_variedad']]): ?>
                                <?php unset($variedades[$_kvarieadad]) ?>
                            <?php endif ?>
                        <?php endforeach ?>
                        <?php echo implode(', ', array_column($variedades, 'variedad_name')) ?>
                    </li>

                    <?php // VARIEDAD ?>
                    <li>Colores:  
                        <?php foreach ($colores as $_kcolor => $_color): ?>
                            <?php if (!$intereses['color'][$_color['id_color']]): ?>
                                <?php unset($colores[$_kcolor]) ?>
                            <?php endif ?>
                        <?php endforeach ?>
                        <?php echo implode(', ', array_column($colores, 'color_name')) ?>
                    </li>
                </ul>
                <h6>¿Ya encontraste tu perrito?</h6>
                <p>Puedes quitarte de nuestra lista de interesados desde el boton "Darme de baja".</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-baja-suscribe" data-suscriber="<?php echo $suscriptor['id_suscriptor'] ?>" data-email="<?php echo $suscriptor['sus_email'] ?>">Darme de baja</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>