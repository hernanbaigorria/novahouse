<div class="modal fade" id="modal-preferencias-suscripcion" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id="frm-actualizar-preferencias">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="font-size: 15px;">Mensaje enviado</h4>
                </div>
                <div class="modal-body">
                        <p>
                            Muchas gracias por escribirnos, recibimos tu mensaje y te contactaremos a la brevedad.
                        </p>
                        <?php if ($is_new): ?>
                            <h6>¿Te gustaría programar una alerta personalizada?</h6>
                            <p>De esta forma cuando tengamos un perrito con las características que te interesan, serás el primero en saberlo.</p>
                        <?php else: ?>
                            <h6>¿Te gustaría actualizar tu alerta personalizada?</h6>
                            <p>De esta forma cuando tengamos el perrito ideal para vos te avisaremos.</p>
                        <?php endif ?>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <h6>¿Que sexo buscas?</h6>
                                <div>
                                    <label class="checkbox-inline fixed">
                                        <input type="hidden" name="sexo[machito]" value="0">
                                        <input type="checkbox" name="sexo[machito]" value="1" <?php if ($intereses['sexo']['machito']) echo 'checked' ?>>
                                        Machito
                                    </label>
                                    <label class="checkbox-inline fixed">
                                        <input type="hidden" name="sexo[hembrita]" value="0">
                                        <input type="checkbox" name="sexo[hembrita]" value="1" <?php if ($intereses['sexo']['hembrita']) echo 'checked' ?>>
                                        Hembrita
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <h6>¿Que pelaje?</h6>
                                <div>
                                    <?php foreach ($variedades as $_kvariedad => $_variedad): ?>
                                    <?php if ($_variedad['id_variedad'] != '1'): ?>
                                    <label class="checkbox-inline fixed">
                                        <input type="hidden" name="variedad[<?php echo $_variedad['id_variedad'] ?>]" value="0">
                                        <input type="checkbox" name="variedad[<?php echo $_variedad['id_variedad'] ?>]" value="1" <?php if ($intereses['variedad'][$_variedad['id_variedad']]) echo 'checked' ?>>
                                        <?php echo $_variedad['variedad_name'] ?>
                                    </label>
                                    <?php endif ?>
                                    <?php endforeach ?>                                
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h6>¿Que color?</h6>
                                <div>
                                    <?php foreach ($colores as $_kcolor => $_color): ?>
                                    <?php if ($_color['id_color'] != '1'): ?>
                                    <label class="checkbox-inline fixed">
                                        <input type="hidden" name="color[<?php echo $_color['id_color'] ?>]" value="0">
                                        <input type="checkbox" name="color[<?php echo $_color['id_color'] ?>]" value="1" <?php if ($intereses['color'][$_color['id_color']]) echo 'checked' ?>>
                                        <?php echo $_color['color_name'] ?>
                                    </label>
                                    <?php endif ?>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="suscriptor" value="<?php echo $id_suscriptor ?>">
                    <button id="btn-cerrar-modal-preferencias" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <?php if ($is_new): ?>
                    <button id="btn-guardar-modal-preferencias" type="submit" class="btn btn-primary">Crear alerta</button>
                    <?php else: ?>
                    <button id="btn-guardar-modal-preferencias" type="submit" class="btn btn-primary">Actualizar alerta</button>
                    <?php endif ?>
                </div>
            </div>
        </form>
    </div>
</div>