<?php 
class Mod_suscriptores_suscriptores extends CI_Model 
{
	
	public function __construct() 
	{

	}

	// Suscriptores

		public function new_suscriptor($sus_email, $sus_telefono)
		{
	        $data['sus_email']              = $sus_email;
	        $data['sus_telefono']              = $sus_telefono;
	        $data['sus_fecha']              = date('Y-m-d H:i:s');

	        $id_suscriptor = $this->already_registered($sus_email, TRUE);
	        if ($id_suscriptor != FALSE)
	        	return $id_suscriptor;

			$result = $this->db->insert('mod_suscriptores_suscriptores', $data);

	        if ($result == TRUE) 
	            return $this->db->insert_id();
	        else
	            return FALSE; 
		}

		public function get_suscriptor($id_suscriptor)
		{
			$cond['id_suscriptor'] = $id_suscriptor;

			$this->db->where($cond);
			$result = $this->db->get('mod_suscriptores_suscriptores');

			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				$result = $result[0];
				return $result;
			}

			return FALSE;
		}

		public function del_suscriptor($id_suscriptor)
		{
			if ($id_suscriptor === FALSE) return FALSE;
			$cond['id_suscriptor'] = $id_suscriptor;

			$this->db->where($cond);
			$result = $this->db->delete('mod_suscriptores_suscriptores');

			return $this->db->affected_rows();
		}

		public function get_suscriptores($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $term_filter = NULL)
		{
			$cond = array();

			$this->db->select('SQL_CALC_FOUND_ROWS mod_suscriptores_suscriptores.*', FALSE);
			$this->db->from('mod_suscriptores_suscriptores');

			if (is_array($filter_column))
			{
				foreach ($filter_column as $key => $value) {
					$cond[$key] = $value;
				}
			}
			else
				if ($filter_column !== FALSE AND $filter_value !== FALSE)
					$cond[$filter_column] = $filter_value;
				else
					if ($filter_column !== FALSE)
						$cond['id_suscriptor'] = $filter_column;

			if (count($cond) > 0)
				$this->db->where($cond);

	        if (!empty($term_filter)) {
	            $this->db->group_start();
	            $this->db->like('sus_email', $term_filter, 'both');
	            $this->db->group_end();
	        }

			if (is_array($order_by))
			{
				foreach ($order_by as $order_column => $sort_order) {
					$this->db->order_by($order_column, $sort_order);
				}
			}

			if ($page !== FALSE)
			{
				$offset = $page*$page_items;
				$this->db->limit($page_items, $offset);
			}
			
			$result = $this->db->get();
			$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();

	            foreach ($result as $_kresult => $_result) 
	            {
					$result[$_kresult]['total_results'] = $paginacion[0]['total_items'];
	            }
				return $result;
			}

			return array();
		}

		public function set_suscriptor($id_suscriptor, $filter_column = FALSE, $filter_value = FALSE)
		{
			$data = array();

			if (is_array($filter_column))
			{
				foreach ($filter_column as $key => $value) {
					$data[$key] = $value;
				}
			}
			else
				if ($filter_column !== FALSE AND $filter_value !== FALSE)
					$data[$filter_column] = $filter_value;

			$cond['id_suscriptor'] = $id_suscriptor;

			$this->db->where($cond);
			$result = $this->db->update('mod_suscriptores_suscriptores', $data);

			return (bool)$this->db->affected_rows();
		}

		public function already_registered($sus_email)
		{
			$cond['mod_suscriptores_suscriptores.sus_email'] 	= $sus_email;
			
			$suscriptores = $this->get_suscriptores($cond, FALSE, 0, 10);

			if (count($suscriptores))
				return $suscriptores[0]['id_suscriptor'];

			return FALSE;
		}

}