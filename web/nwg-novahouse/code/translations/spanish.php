<?php 

// Configuracion del sitio y paginas
	$lang['configurations_site_title']						= 'Título del Sitio';
	$lang['configurations_page_title']						= 'Título de Página';
	$lang['configurations_main_page']						= 'Página Principal';
	$lang['configurations_search_results_page']				= 'Página de resultados de búsqueda (no disponible en esta web)';
	$lang['configurations_page_description']				= 'Descripción de página';
	$lang['configurations_page_tags']						= 'Tags de página';
	$lang['configurations_page_ganalitics']					= 'Código de google analytics';
	$lang['configurations_page_top_menu_opts']				= 'Opciones del menu superior';
	
	$lang['configurations_general_phone']					= 'Número de teléfono a mostrar';
	$lang['configurations_general_address']					= 'Dirección a mostrar';
	$lang['configurations_general_address_latitud']			= 'Latitud';
	$lang['configurations_general_address_longitud']		= 'Longitud';
	$lang['configurations_general_whatsapp']				= 'Número de whatsapp a mostrar';
	$lang['configurations_general_mail']					= 'Dirección de email a mostrar';
	$lang['configurations_general_mail2']					= 'Dirección de email secundaria';
	$lang['configurations_page_contact']					= 'Página de contacto';
	$lang['configurations_general_minmenu']					= 'Menu barra superior';
	$lang['configurations_page_favicon']					= 'Favicon';
	$lang['configurations_logo_header']						= 'Logo header';
	$lang['configurations_logo_header_inverse']				= 'Logo header scrolleado';

	$lang['configurations_body_html']						= 'Cuerpo html';
	$lang['configurations_img_slider']						= 'Imagenes slider';
	$lang['configurations_cantidad_a_mostrar']				= 'Cantidad de imagenes';

	/* COMPONENTE HEADER COLOR */
	$lang['configurations_background_color']				= 'Color de fondo';
	$lang['configurations_button_txt']						= 'Texto del boton';
	$lang['configurations_button_01']						= 'Link del boton';
	$lang['configurations_button_02']						= 'Link del segundo boton';
	$lang['configurations_button_03']						= 'Link del tercer boton';
	$lang['configurations_button_04']						= 'Link del cuarto boton';
	$lang['configurations_button_05']						= 'Link del quinto boton';
	$lang['configurations_button_06']						= 'Link del sexto boton';
	$lang['configurations_button_07']						= 'Link del septimo boton';
	$lang['configurations_button_08']						= 'Link del octavo boton';

	/* COMPONENTE HEADER GRADIENT */
	$lang['configurations_background_color_01']				= 'Color de fondo inicial';
	$lang['configurations_background_color_02']				= 'Color de fondo final';


	/* COMPONENTE HEADER TYPING */
	$lang['configurations_background_image']				= 'Imagen de fondo';
	$lang['configurations_button_txt_01']					= 'Texto del primer boton';
	$lang['configurations_button_txt_02']					= 'Texto del segundo boton';
	$lang['configurations_button_txt_03']					= 'Texto del tecer boton';
	$lang['configurations_button_txt_04']					= 'Texto del cuarto boton';


	$lang['configurations_image_url_01']					= 'Imagen del primer botón';
	$lang['configurations_image_url_02']					= 'Imagen del segundo botón';
	$lang['configurations_image_url_03']					= 'Imagen del tercer botón';

	$lang['configurations_button_url']						= 'Url botón';
	$lang['configurations_button_url_01']					= 'Url del primer botón';
	$lang['configurations_button_url_02']					= 'Url del segundo botón';
	$lang['configurations_button_url_03']					= 'Url del tercer botón';
	

	/* COMPONENTE HEADER BACKGROUND */
	$lang['configurations_titulo_txt']						= 'Titulo fijo';
	$lang['configurations_titulo_txt_variable']				= 'Titulos variables';


	/* COMPONENTE FOOTER 1 */
	$lang['configurations_web_txt']							= 'Nombre de web';
	$lang['configurations_web_link']						= 'Link de la web';
	$lang['configurations_pages_foot']						= 'Paginas del footer';

	/* COMPONENTE FOOTER 2 */
	$lang['configurations_foot_button_txt_01']				= 'Titulo del primer boton';
	$lang['configurations_foot_button_txt_02']				= 'Titulo del segundo boton';
	$lang['configurations_foot_button_link_01']				= 'Link del primer boton';
	$lang['configurations_foot_button_link_02']				= 'Link del segundo boton';


	/* COMPONENTE FEATURE */
	$lang['configurations_image_static']					= 'Imagen';
	$lang['configurations_image_static_01']					= 'Imagen';
	$lang['configurations_image_static_02']					= 'Imagen';
	$lang['configurations_image_static_03']					= 'Imagen';
	$lang['configurations_images_statics']					= 'Imagenes';
	$lang['configurations_background_image_01']				= 'Imagen de fondo 1';
	$lang['configurations_background_image_02']				= 'Imagen de fondo 2';
	$lang['configurations_button_url_video']				= 'Url de video';
	$lang['configurations_icon_01']							= 'Icono 1';
	$lang['configurations_icon_02']							= 'Icono 2';
	$lang['configurations_icon_03']							= 'Icono 3';
	$lang['configurations_icon_04']							= 'Icono 4';
	$lang['configurations_icon_05']							= 'Icono 5';
	$lang['configurations_icon_06']							= 'Icono 6';
	$lang['configurations_icon_07']							= 'Icono 7';
	$lang['configurations_icon_08']							= 'Icono 8';

	$lang['configurations_event_image']						= 'Imagen de evento';
	$lang['configurations_event_link']						= 'Links de eventos';

	$lang['configurations_categorias']						= 'Categorias de noticias';

	$lang['configurations_anuncio_01']						= 'Imagen de anuncio 01';
	$lang['configurations_anuncio_01_link']					= 'Link de anuncio 01';

	$lang['configurations_anuncio_02']						= 'Imagen de anuncio 02';
	$lang['configurations_anuncio_02_link']					= 'Link de anuncio 02';

	$lang['configurations_anuncio_03']						= 'Imagen de anuncio 03';
	$lang['configurations_anuncio_03_link']					= 'Link de anuncio 03';

	$lang['configurations_anuncio_04']						= 'Imagen de anuncio 04';
	$lang['configurations_anuncio_04_link']					= 'Link de anuncio 04';


	$lang['configurations_button_link']						= 'Link';
 
	$lang['configurations_button_link_01']					= 'Link 1';
	$lang['configurations_button_link_02']					= 'Link 2';
	$lang['configurations_button_link_03']					= 'Link 3';


	$lang['configurations_solo_categoria']					= 'Categoria';
	$lang['configurations_solo_destacadas']					= 'Noticias destacadas';


	$lang['configurations_img_header']						= 'Header imagen';

	$lang['configurations_titulo_map']						= 'Titulo de mapa';
	$lang['configurations_latitud']							= 'Latitud';
	$lang['configurations_longitud']						= 'Longitud';
	$lang['configurations_direccion']						= 'Dirección';

	$lang['configurations_activad_01']						= 'Actividad';
	$lang['configurations_activad_02']						= 'Actividad';
	$lang['configurations_activad_03']						= 'Actividad';


	$lang['configurations_img_equipo']						= 'Imagenes del equipo';


	// Ver que estos tienen antepuesto un BR, es como para agruparlos un poco..
	$lang['configurations_page_main_menu_name1']			= '<h3>Menú Principal</h3><strong>Opción 1</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link1']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts1']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name2']			= '<br><strong>Opción 2</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link2']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts2']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name3']			= '<br><strong>Opción 3</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link3']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts3']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name4']			= '<br><strong>Opción 4</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link4']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts4']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name5']			= '<br><strong>Opción 5</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link5']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts5']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name6']			= '<br><strong>Opción 6</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link6']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts6']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name7']			= '<br><strong>Opción 7</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link7']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts7']			= 'Opciones Submenu';
	$lang['configurations_page_main_menu_name8']			= '<br><strong>Opción 8</strong> <br> Rotulo';
	$lang['configurations_page_main_menu_link8']			= 'Link (Opcional)';
	$lang['configurations_page_main_menu_opts8']			= 'Opciones Submenu';

	// Footer tambien tiene tags para que sea mas entendible.
	$lang['configurations_page_foot_intro']					= '<h3>Pie de Página</h3>Texto de intro';
	$lang['configurations_page_foot_menu_t1']				= '<br>Título Columa 1';
	$lang['configurations_page_foot_menu_t2']				= '<br>Título Columa 2';
	$lang['configurations_page_foot_menu_p1']				= '<h3>Menú Footer</h3>';
	$lang['configurations_page_foot_menu_p2']				= 'Links Columa 2';

	// Otras paginas.
	$lang['configurations_page_social_facebook']			= 'Página de Facebook (Externa)';
	$lang['configurations_page_social_twitter']				= 'Página de Twitter (Externa)';
	$lang['configurations_page_social_vimeo']				= 'Página de Vimeo (Externa)';
	$lang['configurations_page_social_instagram']			= 'Página de Instagram (Externa)';
	$lang['configurations_page_social_youtube']				= 'Página de Youtube (Externa)';
	$lang['configurations_page_social_spotify']				= 'Página de Spotify (Externa)';
	$lang['configurations_page_social_linkedin']			= 'Página de Linkedin (Externa)';
	$lang['configurations_page_visible_title']				= 'Título visible de página';
	$lang['configurations_404_page']						= 'Pagina de error 404';