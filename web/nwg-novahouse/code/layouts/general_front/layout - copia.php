    <!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="<?php echo @$PAGE_CFG['page_description'] ?>">
        <meta name="keywords" content="<?php echo @$PAGE_CFG['page_tags'] ?>">

        <link rel="icon" href="<?php echo media_uri($PAGE_CFG['page_favicon']) ?>">
        <title><?php echo @$PAGE_CFG['site_title'] ?><?php echo (!empty($PAGE_CFG['page_title'])) ? ' - '.$PAGE_CFG['page_title'] : NULL ?></title>

        <?php if ($EDITABLE): ?>
            <?php echo $EDITOR_HEADER ?>
        <?php endif ?>

        <link href="<?php echo THEME_ASSETS_URL ?>general/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/custom.css" rel="stylesheet">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        
        <?php if ($EDITABLE): ?>
            <script src="<?php echo static_url('global/plugins/jquery.min.js') ?>"></script>
        <?php else: ?>
            <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <?php endif ?>
    </head>
    <body>
        <div class="topbar">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-2 col-sm-4 hidden-sm-up">
                        <a href="<?php echo base_url(); ?>">
                            <img src="<?php echo THEME_ASSETS_URL ?>general/images/logo-short.svg" width="45">
                        </a>
                    </div>
                    <div class="col-sm-7 hidden-sm-down">
                        <a href="<?php echo base_url(); ?>">
                            <img src="<?php echo THEME_ASSETS_URL ?>general/images/logos-header.svg" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-8 col-sm-8 col-md-5 text-right">
                        <p class="social-icons">
                            <?php $current_language     = get_web_lang(); ?>
                            <?php $available_languages  = $this->config->item('available_languages'); ?>
                            <select class="lang-picker">
                                <?php foreach ($available_languages as $key => $_lang): ?>
                                    <option <?php if ($current_language == $_lang) echo 'selected' ?> data-url="<?php echo goto_lang($_lang) ?>"><?php echo ucfirst($_lang) ?></option>
                                <?php endforeach ?>
                            </select>

                            <?php if (!empty($PAGE_CFG['general_mail'])): ?>
                            <a href="<?php echo page_uri($PAGE_CFG['page_contact']) ?>" class="icon "><i class="fa fa-envelope"></i></a>
                            <?php endif ?>
                            <?php if (!empty($PAGE_CFG['page_social_twitter'])): ?>
                            <a href="<?php echo page_uri($PAGE_CFG['page_social_twitter']) ?>" target="_blank" class="icon marginL"><i class="fa fa-twitter"></i></a>
                            <?php endif ?>
                            <?php if (!empty($PAGE_CFG['page_social_facebook'])): ?>
                            <a href="<?php echo page_uri($PAGE_CFG['page_social_facebook']) ?>" target="_blank" class="icon marginL"><i class="fa fa-facebook"></i></a>
                            <?php endif ?>
                        </p>
                    </div>
                    <div class="col-2 hidden-sm-up">
                        <a href="#menu-mobile" class="" data-toggle="collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="separator hidden-sm-down"></div>
            </div>
        </div>
        <div class="menu desktop hidden-sm-down">
            <div class="container">
                <a href="<?php echo base_url(); ?>" class="logo" style="background-image: url('<?php echo media_uri($PAGE_CFG['logo_header']) ?>');">
                </a>
                <ul>
                <?php // MENU 1 ?>
                <?php 
                    for ($i=1; $i <= 8; $i++) { 
                    ?>
                    <?php $gtp_pages = explode(',', $PAGE_CFG['page_main_menu_opts'.$i]) ?>
                    <?php if (!empty($PAGE_CFG['page_main_menu_name'.$i])): ?>
                        <li class="<?php if ($ID_PAGE == $PAGE_CFG['page_main_menu_link'.$i] OR in_array($ID_PAGE, $gtp_pages)) echo 'active' ?>">
                            <?php 
                                $this_url = page_uri($PAGE_CFG['page_main_menu_link'.$i], $EDITABLE); 
                                $target = (strpos($this_url, base_url()) === FALSE) ? '_blank' : '';
                            ?>
                            <a target="<?php echo $target ?>" href="<?php echo $this_url ?>"><?php echo $PAGE_CFG['page_main_menu_name'.$i] ?></a>
                            <?php if (count($gtp_pages) >= 1 AND !empty($gtp_pages[0])): ?>
                                <div class="sub-menu">
                                    <ul>
                                        <?php foreach ($gtp_pages as $key => $gtp_page): ?>
                                            <li class="<?php if ($ID_PAGE == $gtp_page) echo 'active' ?>">
                                                <?php 
                                                    $this_url = page_uri($gtp_page, $EDITABLE); 
                                                    $target = (strpos($this_url, base_url()) === FALSE) ? '_blank' : '';
                                                ?>
                                                <a target="<?php echo $target ?>" href="<?php echo page_uri($gtp_page, $EDITABLE) ?>"><?php echo page_title($gtp_page, $LANG) ?></a>
                                            </li><br>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            <?php endif ?>
                        </li> 
                    <?php endif ?>
                    <?php 
                    }
                ?>
                </ul>
            </div>
        </div>
        <div class="menu mobile collapse" id="menu-mobile">
            <ul>
                <?php // MENU 1 ?>
                <?php 
                    for ($i=1; $i <= 8; $i++) { 
                    ?>
                    <?php $gtp_pages = explode(',', $PAGE_CFG['page_main_menu_opts'.$i]) ?>
                    <?php if (!empty($PAGE_CFG['page_main_menu_name'.$i])): ?>
                        <li class="<?php if ($ID_PAGE == $PAGE_CFG['page_main_menu_link'.$i] OR in_array($ID_PAGE, $gtp_pages)) echo 'active' ?>">
                            <?php if (count($gtp_pages) >= 1 AND !empty($gtp_pages[0])): ?>
                                <a href="#smenu-<?php echo $PAGE_CFG['page_main_menu_link'.$i]; ?>" data-toggle="collapse"><?php echo $PAGE_CFG['page_main_menu_name'.$i] ?>&nbsp;&nbsp;<i class="fa fa-caret-down"></i> </a>
                                <div class="sub-menu collapse" id="smenu-<?php echo $PAGE_CFG['page_main_menu_link'.$i]; ?>">
                                    <ul>
                                        <?php foreach ($gtp_pages as $key => $gtp_page): ?>
                                            <li class="<?php if ($ID_PAGE == $gtp_page) echo 'active' ?>">
                                                <a href="<?php echo page_uri($gtp_page, $EDITABLE) ?>"><?php echo page_title($gtp_page, $LANG) ?></a>
                                            </li><br>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            <?php else: ?>
                                <a href="<?php echo page_uri($PAGE_CFG['page_main_menu_link'.$i], $EDITABLE) ?>"><?php echo $PAGE_CFG['page_main_menu_name'.$i] ?></a>
                            <?php endif ?>
                        </li> 
                    <?php endif ?>
                    <?php 
                    }
                ?>
            </ul>
        </div>

        <div class="clear-gp-fix"></div>

        <?php if ($PAGE_CFG['header_deco_enable']): ?>
        <div class="top-decoration">
            <div class="container innerB inner-2x">
                <img src="<?php echo THEME_ASSETS_URL ?>general/images/guarda.svg" class="col-12 col-xs-12 padding-none">
            </div>
        </div>
        <?php endif ?>

        <?php echo @$MODULE_BODY; ?>

        <?php if ($PAGE_CFG['side_menu_enable']): ?>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-3">
                        <ul class="side-menu">
                            <?php $gtp_pages_bk = explode(',', $PAGE_CFG['side_menu_back']) ?>
                            <?php $gtp_pages = explode(',', $PAGE_CFG['side_menu']) ?>

                            <?php foreach ($gtp_pages_bk as $key => $it_page): ?>
                            <?php if (!empty($it_page)): ?>
                                <li class="<?php if ($ID_PAGE == $it_page) echo 'active' ?>">
                                    <a href="<?php echo page_uri($it_page, $EDITABLE) ?>">
                                        <i class="fa fa-angle-left pull-left innerR"></i>
                                        <strong><?php echo page_title($it_page, $LANG) ?></strong>
                                    </a>
                                </li>
                            <?php endif ?>
                            <?php endforeach ?>                                

                            <?php // Mostramos la opcion de menu madre de la pagina que se esta mostrando.  ?>
                            <?php if (FALSE): ?>
                                <?php if ($page['parent_page'] !== FALSE ): ?>
                                    <li class="<?php if ($ID_PAGE == $page) echo 'active' ?>">
                                        <a href="<?php echo page_uri($page['parent_page']['id_page'], $EDITABLE) ?>">
                                            <strong><?php echo page_title($page['parent_page']['id_page'], $LANG) ?></strong>
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php // O esta misma como activa si fuese la que se muestra ?>
                                <?php if ($page['is_section'] == TRUE AND $page['id_page'] == $ID_PAGE AND !in_array($ID_PAGE, $gtp_pages)): ?>
                                    <li class="<?php echo 'active' ?>">
                                        <a href="<?php echo page_uri($ID_PAGE, $EDITABLE) ?>">
                                            <strong><?php echo page_title($ID_PAGE, $LANG) ?></strong>
                                        </a>
                                    </li>
                                <?php endif ?>
                            <?php endif ?>
                            
                            <?php foreach ($gtp_pages as $key => $it_page): ?>
                                <?php if (!empty($it_page)): ?>
                                    <?php $page_childs = page_childs($it_page); ?>
                                    <li class="<?php if ($ID_PAGE == $it_page) echo 'active' ?>">
                                        <a href="<?php echo page_uri($it_page, $EDITABLE) ?>">
                                            <strong><?php echo page_title($it_page, $LANG) ?></strong>
                                            <?php if (count($page_childs) > 0): ?>
                                            <i class="fa fa-angle-right"></i>
                                            <?php endif ?>
                                        </a>
                                        <?php if (count($page_childs) > 0): ?>
                                            <ul>
                                                <?php foreach ($page_childs as $skey => $s_page): ?>
                                                <li>
                                                    <a href="<?php echo page_uri($s_page['id_page'], $EDITABLE) ?>">
                                                        <strong><?php echo page_title($s_page['id_page'], $LANG) ?></strong>
                                                    </a>
                                                </li>
                                                <?php endforeach ?>
                                            </ul>
                                        <?php endif ?>
                                    </li>
                                <?php endif ?>
                            <?php endforeach ?>                                
                        </ul>
                    </div>
                    <div class="col-1 hidden-sm-down">
                        
                    </div>
                    <div class="col-12 col-sm-8">
                        <div class="page_components_containers">
                            <?php render_components($ID_PAGE, $LANG, $VERSION, $EDITABLE) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php else: ?>
            <div class="page_components_containers">
                <?php render_components($ID_PAGE, $LANG, $VERSION, $EDITABLE) ?>
            </div>
        <?php endif ?>

        <div class="footer">
            <div class="container">
                <div class="row row-eq-height">
                    <div class="col-md-5">
                        <ul class="list-details">
                            <?php if (!empty($PAGE_CFG['general_evdate'])): ?>
                            <li><i class="fa fa-calendar"></i><?php echo $PAGE_CFG['general_evdate'] ?></li>
                            <?php endif ?>
                            <?php if (!empty($PAGE_CFG['general_address'])): ?>
                            <li><i class="fa fa-map-marker"></i><?php echo $PAGE_CFG['general_address'] ?></li>
                            <?php endif ?>
                            <?php if (!empty($PAGE_CFG['general_phone'])): ?>
                            <li><i class="fa fa-phone"></i><?php echo $PAGE_CFG['general_phone'] ?></li>
                            <?php endif ?>
                            <?php if (!empty($PAGE_CFG['general_mail'])): ?>
                            <li><i class="fa fa-envelope"></i><?php echo $PAGE_CFG['general_mail'] ?></li>
                            <?php endif ?>
                            <?php if (!empty($PAGE_CFG['general_mail2'])): ?>
                            <li><i class="fa fa-envelope"></i><?php echo $PAGE_CFG['general_mail2'] ?></li>
                            <?php endif ?>
                        </ul>
                    </div>
                    <div class="col-md-2 text-center">
                        <p style="padding-top: 35px;" class="social-icons">
                            <?php if (!empty($PAGE_CFG['general_mail'])): ?>
                            <a href="mailto:<?php echo $PAGE_CFG['general_mail'] ?>" class="icon "><i class="fa fa-envelope"></i></a>
                            <?php endif ?>
                            <?php if (!empty($PAGE_CFG['page_social_twitter'])): ?>
                            <a href="<?php echo page_uri($PAGE_CFG['page_social_twitter']) ?>" target="_blank" class="icon marginL"><i class="fa fa-twitter"></i></a>
                            <?php endif ?>
                            <?php if (!empty($PAGE_CFG['page_social_facebook'])): ?>
                            <a href="<?php echo page_uri($PAGE_CFG['page_social_facebook']) ?>" target="_blank" class="icon marginL"><i class="fa fa-facebook"></i></a>
                            <?php endif ?>
                        </p>
                    </div>
                    <div class="col-md-4 offset-md-1">
                        <?php $gtp_pages = explode(',', $PAGE_CFG['page_foot_menu_p1']) ?>
                        <?php if (count($gtp_pages) >= 1 AND !empty($gtp_pages[0])): ?>
                            <ul class="footer-links">
                                <?php foreach ($gtp_pages as $key => $gtp_page): ?>
                                    <li><a href="<?php echo page_uri($gtp_page) ?>"><?php echo page_title($gtp_page, $LANG) ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal-asistir" tabindex="-1" role="dialog" aria-labelledby="mod-asistir" aria-hidden="true" style="z-index: 99999;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mod-asistir"><?php echo $PAGE_CFG['mod_asis_title'] ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo $PAGE_CFG['mod_asis_body'] ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $PAGE_CFG['mod_asis_btn'] ?></button>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($EDITABLE): ?>
            <?php echo $EDITOR_FOOTER ?>
        <?php else: ?>
            
        <?php endif ?>

        <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js" data-wtf="Dependencia de BS4"></script>
        <script src="<?php echo THEME_ASSETS_URL ?>general/js/bootstrap.min.js"></script>
        <script src="<?php echo THEME_ASSETS_URL ?>general/js/custom.js"></script>
    </body>
</html>