<!DOCTYPE html>
<html dir="ltr" lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="<?php echo @$PAGE_CFG['page_description'] ?>">
        <meta name="keywords" content="<?php echo @$PAGE_CFG['page_tags'] ?>">

        <link rel="icon" href="<?php echo media_uri($PAGE_CFG['page_favicon']) ?>">
        <title><?php echo @$PAGE_CFG['site_title'] ?><?php echo (!empty($PAGE_CFG['page_title'])) ? ' - '.$PAGE_CFG['page_title'] : NULL ?></title>

        <?php if ($EDITABLE): ?>
            <?php echo $EDITOR_HEADER ?>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <link href="<?php echo THEME_ASSETS_URL ?>general/css/bootstrap.min.css" rel="stylesheet">
        <?php endif ?>

        
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        
        <?php if ($EDITABLE): ?>
            <script src="<?php echo static_url('global/plugins/jquery.min.js') ?>"></script>
        <?php else: ?>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <?php endif ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css" />
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/custom.css" rel="stylesheet">
    </head>
    <body class="">
        <div id="wrapper" class="clearfix" style="margin: 0 5px;">
            <!-- Header -->
            <header id="header" class="back-header">
                <div class="col-12">
                    <div class="row align-items-center">
                        <div class="col-12 col-sm-3"></div>
                        <div class="col-12 col-sm-6 text-center">
                            <img src="<?php echo media_uri($PAGE_CFG['logo_header']) ?>" class="img-fluid logo-header">
                        </div>
                        <div class="col-12 col-sm-3">
                            <a href="#" class="d-flex align-items-center justify-content-center flex-wrap btn-contacto justify-content-center align-items-center">
                                <div class="info-btn">
                                    <h3>CONTACTANOS</h3>
                                    <p>PARA MÁS INFORMACIÓN</p>
                                </div>
                                <img src="<?php echo THEME_ASSETS_URL ?>general/images/logo_msj.png" class="icon-msj">
                            </a>
                        </div>
                    </div>
                </div>
            </header>
            <!-- Start main-content -->
            <div class="page_components_containers">
                <?php render_components($ID_PAGE, $LANG, $VERSION, $EDITABLE) ?>
            </div>

            <?php echo @$MODULE_BODY; ?>
            <!-- end main-content -->

            <!-- Footer -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-4 d-flex justify-content-center align-items-center flex-wrap border-right-foot">
                            <?php if(!empty($PAGE_CFG['general_phone'])): ?>
                            <img src="<?php echo THEME_ASSETS_URL ?>general/images/icon_wpp.png" class="icon-wpp">
                            <p><?php echo $PAGE_CFG['general_phone'] ?></p>
                            <?php endif ?>
                        </div>
                        <div class="col-12 col-sm-4 text-center">
                            <img src="<?php echo media_uri($PAGE_CFG['logo_header']) ?>" class="img-fluid logo-foot">
                        </div>
                        <div class="col-12 col-sm-4 d-flex justify-content-center align-items-center flex-wrap border-left-foot">
                            <a href="http://www.novahouse.com.ar/" target="_blank">www.novahouse.com.ar</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <?php if ($EDITABLE): ?>
            <?php echo $EDITOR_FOOTER ?>
            <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js" data-wtf="Dependencia de BS4"></script>
            <script src="<?php echo THEME_ASSETS_URL ?>general/js/bootstrap.min.js"></script>

        <?php else: ?>
            
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <?php endif ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        <script type="text/javascript">
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:25,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:3
                    }
                }
            })
        </script>
        <script src="<?php echo THEME_ASSETS_URL ?>general/js/custom.js"></script>
    </body>
</html>