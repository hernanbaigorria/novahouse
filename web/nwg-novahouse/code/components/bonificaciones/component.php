<div class="row" style="margin:0 -20px">
    <div class="col-12 col-sm-5 text-right p-0">
        <div class="content-color-04">
            <div class="max-content">
                <?php gtp_paragraph('txt-title', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                <?php gtp_paragraph('txt-descrpition', 'p', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                <img src="<?php echo THEME_ASSETS_URL ?>general/images/logo_box.png" class="absolute-logo">
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-7  p-0" style="overflow:hidden;">
        <div class="back-color-02" style="background: url(<?php echo media_uri($COMPONENT_CFG['background_image']) ?>); "></div>
    </div>
</div>


