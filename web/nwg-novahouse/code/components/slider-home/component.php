<div id="carouselExampleIndicators" class="carousel slide slide-home" data-ride="carousel">
  <ol class="carousel-indicators">
    <?php if (!empty($COMPONENT_CFG['images_statics'])): ?>
    <?php $slider_images = explode(',' , $COMPONENT_CFG['images_statics']) ?>
      <?php foreach ($slider_images as $_kimages => $_id_images): ?>
        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $_kimages ?>" class="<?php if($_kimages == 0): ?> active <?php endif; ?>"></li>
      <?php endforeach; ?>
    <?php endif; ?>
  </ol>
  <div class="carousel-inner">
    <?php if (!empty($COMPONENT_CFG['images_statics'])): ?>
    <?php $slider_images = explode(',' , $COMPONENT_CFG['images_statics']) ?>
      <?php foreach ($slider_images as $_kimages => $_id_images): ?>
        <div class="carousel-item <?php if($_kimages == 0): ?> active <?php endif; ?>">
          <img src="<?php echo media_uri($_id_images) ?>" class="img-fluid" style="border-radius: 30px;">
        </div>
      <?php endforeach; ?>
    <?php endif; ?>
    
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>