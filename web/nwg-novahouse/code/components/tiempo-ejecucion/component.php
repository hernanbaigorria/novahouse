<div class="row" style="margin:0 -20px">
    
    <div class="col-12 col-sm-6 p-0" style="overflow:hidden;">
        <div class="back-color-03" style="background: url(<?php echo media_uri($COMPONENT_CFG['background_image']) ?>); "></div>
    </div>
    <div class="col-12 col-sm-6 text-left p-0">
        <div class="content-color-03">
            <?php gtp_paragraph('txt-title', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            <?php gtp_paragraph('txt-descrpition', 'p', $ID_COMPONENT, $LANG, $EDITABLE) ?>
        </div>
    </div>
</div>

