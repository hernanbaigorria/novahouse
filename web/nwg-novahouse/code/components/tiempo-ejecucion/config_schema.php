<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$ADDED_CFG['background_image']['default']				= NULL;
	$ADDED_CFG['background_image']['multilang'] 			= TRUE;
	$ADDED_CFG['background_image']['permissions'] 			= FALSE;
	$ADDED_CFG['background_image']['type'] 					= 'image';
	$ADDED_CFG['background_image']['max'] 					= 1;