<div class="info-slider">
    <div class="container text-center">
        <div class="row">
            <div class="col-12 col-sm-2"></div>
            <div class="col-12 col-sm-8">
                <?php gtp_paragraph('txt-description', 'p', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            </div>
            <div class="col-12 col-sm-2"></div>
        </div>
    </div>
</div>

