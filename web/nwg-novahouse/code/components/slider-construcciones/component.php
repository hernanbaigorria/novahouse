<section class="slider-construcciones">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <?php gtp_paragraph('txt-title', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            </div>
            <div class="col-12 col-sm-2"></div>
            <div class="col-12 col-sm-8">
                <div class="owl-carousel owl-theme" style="margin-bottom: 30px;">
                    <?php if (!empty($COMPONENT_CFG['images_statics'])): ?>
                    <?php $slider_images = explode(',' , $COMPONENT_CFG['images_statics']) ?>
                      <?php foreach ($slider_images as $_kimages => $_id_images): ?>
                        <div class="item">
                            <img src="<?php echo media_uri($_id_images) ?>" class="img-fluid">
                        </div>
                       <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-sm-2"></div>
            <div class="col-12 text-center">
                <a href="#" class="d-inline-flex align-items-center justify-content-center flex-wrap btn-contacto justify-content-center align-items-center">
                    <img style="max-width: 23px;" src="<?php echo THEME_ASSETS_URL ?>general/images/flecha_icon.png" class="icon-msj">
                    <div class="info-btn" style="display:inline-block;padding: 0;margin: 0 15px;">
                        <h3 style="margin:0">MÁS INFORMACIÓN</h3>
                        <p>En 60 días obtené tu casa.</p>
                    </div>
                    <img style="transform:rotate(-180deg);max-width: 23px;" src="<?php echo THEME_ASSETS_URL ?>general/images/flecha_icon.png" class="icon-msj">
                </a>
            </div>
        </div>
    </div>
</section>