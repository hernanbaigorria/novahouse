<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$ADDED_CFG['images_statics']['default']						= NULL;
	$ADDED_CFG['images_statics']['multilang'] 					= TRUE;
	$ADDED_CFG['images_statics']['permissions'] 				= FALSE;
	$ADDED_CFG['images_statics']['type'] 						= 'image';
	$ADDED_CFG['images_statics']['max'] 						= 9999;