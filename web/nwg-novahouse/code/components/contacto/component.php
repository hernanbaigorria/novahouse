<div class="back-contacto-top" style="background:url(<?php echo media_uri($COMPONENT_CFG['background_image']) ?>);"></div>
<div class="content-formulario">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-1"></div>
            <div class="col-12 col-sm-3">
                <?php gtp_paragraph('txt-title', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            </div>
            <div class="col-12 col-sm-4">
                <form class="frm-suscribe newsletter-form">
                    <div class="input-group">
                        <input  type="text" name="nombre" placeholder="Nombre y Apellido">
                        <input  type="number" name="telefono" placeholder="Teléfono">
                        <input required type="email" name="email" placeholder="Mail">
                        <input  type="text" name="metros" placeholder="¿Cuántos metros tiene el terreno?">
                        <textarea name="consulta" placeholder="Consulta"></textarea>
                        <input type="submit" name="enviar" value="ENVIAR">
                    </div>
                    <p class="lbl-suscripto hidden">Gracias, ¡Estaremos en contacto!</p>
                    <p class="lbl-error-suscripto hidden">Ocurrió un error. Intente nuevamente.</p>
                </form>
                
            </div>
            <div class="col-12 col-sm-3" style="margin-top:auto">
                <?php gtp_paragraph('txt-title-02', 'h4', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            </div>
            <div class="col-12 col-sm-1"></div>
        </div>
    </div>
</div>