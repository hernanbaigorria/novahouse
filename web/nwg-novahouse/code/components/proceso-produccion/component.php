<div class="row" style="margin:0 -20px">
    <div class="col-12 col-sm-5 text-right p-0">
        <div class="content-color-01">
            <?php gtp_paragraph('txt-title', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            <?php gtp_paragraph('txt-descrpition', 'p', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            <img src="<?php echo media_uri($PAGE_CFG['logo_header']) ?>" class="img-fluid logo">
        </div>
        <div class="content-color-02">
            <?php gtp_paragraph('txt-title-02', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
        </div>
    </div>
    <div class="col-12 col-sm-7  p-0" style="overflow:hidden;">
        <div class="back-color-02" style="background: url(<?php echo media_uri($COMPONENT_CFG['background_image']) ?>); "></div>
    </div>
</div>

