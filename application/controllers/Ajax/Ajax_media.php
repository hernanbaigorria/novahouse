<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_media extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        if(!$this->input->is_ajax_request()) 
			show_404();

		define('PAGE', 'media');
		$this->lang->load('media_lang', $this->session->userdata('user_language'));
    }

    // Gestion de elementos 

		// Esta funcion recibe la cantidad elementos que se estan mostrando en la galeria, y devuelve un set de elementos siguientes.
		// Se utiliza para mostrar mas elementos ne la galeria.
		public function get_more_media()
		{
			$offset 		= $this->input->post('offset');
			$pickable 		= $this->input->post('pickable');
			$media_type		= $this->input->post('filter');
			$selecteds 		= (string)$this->input->post('selecteds');
			$name_filter	= $this->input->post('name_filter');
			$group_filter	= $this->input->post('group_filter');
			
			$media_items 	= $this->media->get_media(FALSE, FALSE, ($offset/PAGINATION_MEDIA_LIST), PAGINATION_MEDIA_LIST, $media_type, $selecteds, $name_filter, $group_filter);
			$SELECCIONADOS = explode(',', $selecteds);
			
			$feed = NULL;
			foreach ($media_items as $key => $item)
			{
				$item['selected'] = (in_array($item['id_media'], $SELECCIONADOS));
				$feed .= ($pickable == 1) ? load_page_piece('media_item_pickable', $item, TRUE) : load_page_piece('media_item', $item, TRUE);
			}

			if (count($media_items) > 0) 
			{
				$ext['feed'] = $feed;
				ajax_response('success', $this->lang->line('general_everything_ok'), '1', '0', $ext);
			}
			else
			{
				ajax_response('success', $this->lang->line('media_no_more_items'), '2', '1');
			}
		}

		// Esta funcion genera un modal para elegir imagenes o archivos.
		// 
		public function get_media_picker()
		{
			$data['cfg_item'] 	= $this->input->post('cfg');
			$data['media_type'] = $this->input->post('filter');
			$data['max_items'] 	= (int)$this->input->post('max');
			$data['selecteds'] 	= $this->input->post('selecteds');

			$ext['composer'] 	= $this->load->view('nawglobe/components/gtp_media_picker', $data, TRUE);
			echo ajax_response('success', 'Se puede seleccionar...', 1, 0, $ext);
		}

		// Esta funcion se utiliza para los uploads, esta conectada con el plugin de fileupload.js
		public function upload_files()
		{
			// Guardamos todos los archivos subidos en la carpeta media. Se crean los thumbs automaticamente.
			require_once './application/libraries/Upload_library.php';
			$options['upload_dir'] = PATH_UPLOADS;
			$options['upload_url'] = uploads_url('media/');
			$options['media_uploader'] = $this->session->userdata('id_user');
			$upload_handler = new UploadHandler($options);
		}

		// Actualzia la info de un archivo
		public function update_media()
		{
	    	$media_name 		= $this->input->post('media_name');
	    	$media_file_name 	= $this->input->post('media_file_name');
	    	$id_media 			= $this->input->post('id_media');
	    	$media_group 		= $this->input->post('media_group');

			// Controlamos que el usuario tenga permiso de gestionar usuarios
			check_permissions('gestorp', 'manage_media', FALSE, TRUE);
			check_permissions('gestorp', 'edit_files', FALSE, TRUE);

			if (strlen($media_file_name) < 2)
				ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');

			if (empty($media_name)) $media_name = NULL;

			// Actualizamos el nombre de archivo.
			$media 			= $this->media->get_file($id_media);
			$media_meta 	= pathinfo($media['media_file_name']);

			$media_old_name = PATH_UPLOADS.$media['media_file_name'];
			$media_new_name = PATH_UPLOADS.$media_file_name.'.'.$media_meta['extension'];

			$media_t_old_name = PATH_THUMBNAILS.$media['media_file_name'];
			$media_t_new_name = PATH_THUMBNAILS.$media_file_name.'.'.$media_meta['extension'];

			$rename_result 	= @rename($media_old_name, $media_new_name);
			if ($rename_result == TRUE)
			{
				// Actualizamos el thumb tambien.
				@rename($media_t_old_name, $media_t_new_name);
				$this->media->set_file($id_media, 'media_file_name', $media_file_name.'.'.$media_meta['extension']);
			}
			else
			{
				ajax_response('danger', $this->lang->line('general_error_file_exist'), '0', '1');
			}

			// Actualizamos el nombre para mostrar.
			$this->media->set_file($id_media, 'media_name', $media_name);
			$this->media->set_file($id_media, 'media_group', $media_group);

			$ext['action'] = 'redirect';
			$ext['redirect_delay'] 	= '500';
			$ext['redirect_target'] = "?action=gallery";
			ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);

		}

		public function replace_media()
		{
			check_permissions('gestorp', 'replace_files', FALSE, TRUE);

			$id_media 	= $this->input->post('id_media');
			$id_user 	= $this->session->userdata('id_user');

			$config['upload_path'] 		= PATH_UPLOADS;
			$config['allowed_types'] 	= 'gif|jpg|jpeg|png|mp4|pdf|GIF|JPG|JPEG|PNG|MP4|PDF';
			$config['encrypt_name'] 	= TRUE;
			$this->load->library('upload', $config);
			
			$result = $this->upload->do_upload('file_replacement');
			
			if ($result == TRUE) 
			{
				$old_file = $this->media->get_file($id_media);
				$new_file = $this->upload->data();
				
				// Verificamos que sean archivos del mismo tipo, sino cancelamos.
				if ($new_file['file_type'] != $old_file['media_type'])
				{
					@unlink(PATH_UPLOADS.$new_file['file_name']);
					ajax_response('danger', $this->lang->line('media_replace_file_type_err'), '0', '1');
				}

				// Actualozamos el regstro en la db
					$this->media->set_file($id_media, 'media_type', 		$new_file['file_type']);
					$this->media->set_file($id_media, 'media_file_size', 	$new_file['file_size']*1024);
					$this->media->set_file($id_media, 'upload_date', 		date('Y-m-d H:i:s'));
					$this->media->set_file($id_media, 'media_uploader', 	$id_user);

				// Eliminamos el archivo original. Y thumb si es necesario.
	    			@unlink(PATH_UPLOADS.$old_file['media_file_name']);
	    			@unlink(PATH_THUMBNAILS.$old_file['media_file_name']);

				// Movemos el nuevo al lugar del original.
	    			@rename(PATH_UPLOADS.$new_file['file_name'], PATH_UPLOADS.$old_file['media_file_name']);

				// Creamos thumbs si es necesario.
	    			if ($new_file['is_image'] == TRUE) {
				        $cfg_resize = array(
				            'source_image' => PATH_UPLOADS.$old_file['media_file_name'],
				            'new_image' => PATH_THUMBNAILS.$old_file['media_file_name'],
				            'maintain_ratio' => true,
				            'width' => 500,
				            'height' => 500
				        );

				        $this->load->library('image_lib', $cfg_resize);
				        $this->image_lib->resize();
	    			}

				// Salimos
					$ext['action']          = 'reload';
	                $ext['reload_delay']    = '500';
	                ajax_response('success', $this->lang->line('pages_v_published_succesfully'), '1', '1', $ext);
			}
			else
			{
				ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
			}
		}

		public function remove_media()
		{
			check_permissions('gestorp', 'remove_files', FALSE, TRUE);
	    	$id_media 	= $this->input->post('id_media');
			
	    	// Eliminamos el media file, 
	    		$result = $this->media->del_file($id_media);

	    	// Si todo esta OK, eliminamos las referencias que pueda haber a este archivo. O salimos con error.
	    		if ($result == TRUE)
	    			$this->collections->clean_references('media_items', $id_media);
	    		else
					ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');

			$ext['action'] = 'redirect';
			$ext['redirect_delay'] 	= '500';
			$ext['redirect_target'] = "?action=gallery";
			ajax_response('success', $this->lang->line('media_removed_succesfully'), '1', '1', $ext);
		}

		public function get_previews()
		{
			$source = $this->input->post('source');
			$return = NULL;

			// Si tenemos elementos,
			if (strlen($source) > 0) 
			{
				$source = explode(',', $source);

				foreach ($source as $key => $item) {
					$file 	= media_uri($item, TRUE);
					$title 	= media_title($item);
					$return .= "<img height='34' data-id='$item' src='$file' title='$title'>";
				}
			}

			$ext['preview_data'] 	= $return;
			echo ajax_response('success', '...', 1, 0, $ext);
		}

	// Gestion de Grupos

		public function create_group()
		{
			$group_name = $this->input->post('group_name');

			$id_group = $this->media->new_group($group_name);

			if ($id_group !== FALSE)
			{
				$ext['action'] = 'redirect';
				$ext['redirect_delay'] 	= '500';
				$ext['redirect_target'] = base_url().GESTORP_MANAGER.'/media?action=manage_groups';
				ajax_response('success', $this->lang->line('media_group_created_succesfully'), '1', '1', $ext);
			}
			else
				ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
		}

		public function remove_media_group()
		{
			$id_group = $this->input->post('id_group');

			$result = $this->media->del_group($id_group);

			if ($result == TRUE)
			{
				$ext['action'] = 'redirect';
				$ext['redirect_delay'] 	= '500';
				$ext['redirect_target'] = base_url().GESTORP_MANAGER.'/media?action=manage_groups';
				ajax_response('success', $this->lang->line('media_group_removed_succesfully'), '1', '1', $ext);
			}
			else
				ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
		}

		public function update_group()
		{
			$id_group 	= $this->input->post('id_group');
			$group_name = $this->input->post('group_name');

			$result = $this->media->set_group($id_group, 'group_name', $group_name);

			if ($result !== 0)
			{
				$ext['action'] = 'redirect';
				$ext['redirect_delay'] 	= '500';
				$ext['redirect_target'] = base_url().GESTORP_MANAGER.'/media?action=manage_groups';
				ajax_response('success', $this->lang->line('media_group_updated_succesfully'), '1', '1', $ext);
			}
			else
				ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
		}
}
