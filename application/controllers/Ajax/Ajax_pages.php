<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_pages extends CI_Controller {

        function __construct() 
        {
            parent::__construct();
            
            if(!$this->input->is_ajax_request()) 
    			show_404();
            
            $this->control_library->session_check();

    		$this->lang->load('pages_lang', $this->session->userdata('user_language'));
        }

    	public function create_page()
    	{
            // Controlamos que el usuario tenga permiso para crear paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

    		$page_type		= $this->input->post('page_type');
        	$page_title		= $this->input->post('page_title');
        	$page_parent	= (int)$this->input->post('page_parent');
            $page_slug      = $this->input->post('page_slug');
        	$page_url		= $this->input->post('page_url');
            $page_layout    = $this->input->post('page_layout');
            $preset	        = $this->input->post('preset');

        	if (strlen($page_title) < 3) 
                ajax_response('danger', $this->lang->line('pages_error_short_title'), '0', '1');

            switch ($page_type) {
                case 'page':
                    if (empty($preset)) $preset = 'simples';
                    if (strlen($page_slug) < 3)     ajax_response('danger', $this->lang->line('pages_error_short_slug'), '0', '1');
                    if (empty($page_layout))        ajax_response('danger', $this->lang->line('pages_error_no_layout'), '0', '1');
                    if (!$this->pages->chk_slug($page_slug, FALSE, FALSE, 'page')) ajax_response('danger', $this->lang->line('pages_error_uri_used'), '0', '1');
                    $id_page = $this->pages->new_page(FALSE, FALSE, 0, $page_title, slug($page_slug), 1, $page_layout);
                    $this->pages->new_version($id_page);
                    $this->configurations->reset_configuration('page', $id_page);
                    break;
                case 'section':
                    // Controles
                    if (empty($preset)) 
                        $preset = 'grouped';
                    if (strlen($page_slug) < 3)     
                        ajax_response('danger', $this->lang->line('pages_error_short_slug'), '0', '1');
                    if (empty($page_layout))        
                        ajax_response('danger', $this->lang->line('pages_error_no_layout'), '0', '1');
                    if (!$this->pages->chk_slug($page_slug, FALSE, FALSE, 'section')) 
                        ajax_response('danger', $this->lang->line('pages_error_uri_used'), '0', '1');
                    
                    $id_page = $this->pages->new_page(TRUE, FALSE, 0, $page_title, slug($page_slug), 1, $page_layout);
                    $this->pages->new_version($id_page);
                    $this->configurations->reset_configuration('page', $id_page);
                    break;
                case 'external_page':
                    if (empty($preset)) $preset = 'external';
                    if (strlen($page_url) < 3)      ajax_response('danger', $this->lang->line('pages_error_short_slug'), '0', '1');
                    $id_page = $this->pages->new_page(FALSE, TRUE, 0, $page_title, $page_url, 1, NULL);
                    break;
                case 'section_page':
                    if (empty($preset)) $preset = 'grouped';
                    if (strlen($page_slug) < 3)     ajax_response('danger', $this->lang->line('pages_error_short_slug'), '0', '1');
                    if ($page_parent == 0)          ajax_response('danger', $this->lang->line('pages_error_no_parent'), '0', '1');
                    if (!$this->pages->chk_slug($page_slug, $page_parent)) ajax_response('danger', $this->lang->line('pages_error_uri_used'), '0', '1');
        			$id_page = $this->pages->new_page(FALSE, FALSE, $page_parent, $page_title, slug($page_slug), 1, NULL);
                    $this->pages->new_version($id_page);
        			break;
        		default:
        			ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        			break;
        	}

            if (empty($id_page)) {
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
            }

            // Le damos permiso al usuario creador para editar esta pagina.
            $id_user = $this->session->userdata('id_user');
            $this->permissions->set($id_user, 'pages', $id_page, TRUE);

    		$ext['action'] = 'redirect';
    		$ext['redirect_delay'] 	= '500';
    		$ext['redirect_target'] = base_url().GESTORP_MANAGER.'/pages/'.$preset.'?action=edit&id='.$id_page;
    		ajax_response('success', $this->lang->line('pages_created_succesfully'), '1', '1', $ext);
    	}

        public function update_page()
        {
            // Controlamos que el usuario tenga permiso para crear paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

            $id_page            = $this->input->post('id_page');
            $page_title         = $this->input->post('page_title');
            $page_slug          = $this->input->post('page_slug');
            $page_cache_time    = $this->input->post('page_cache_time');
            $id_parent_page     = $this->input->post('id_parent_page');
            
            // Controlamos que el usuario tenga permiso de gestionar esta pagina.
                check_permissions('pages', $id_page, FALSE, TRUE);

            // Controlamos 
            if (strlen($page_title) <= 3)   ajax_response('danger', $this->lang->line('pages_error_short_title'), '0', '1');
            $page = $this->pages->get_page($id_page);
            
            // Si no es una pagina externa, hacemos los controles.
            if (!$page['is_external']) 
            {
                // Controlamos longitudes aceptables.
                if (strlen($page_slug) <= 3)    ajax_response('danger', $this->lang->line('pages_error_short_slug'), '0', '1');

                // Controlamos que el slug sea correcto.
                $page_slug = slug($page_slug);
                if (!$this->pages->chk_slug($page_slug, $id_parent_page, $id_page)) ajax_response('danger', $this->lang->line('pages_error_uri_used'), '0', '1');
            }

            // Realizamos el update.
            $this->pages->set_page($id_page, 'page_title', $page_title);
            $this->pages->set_page($id_page, 'page_slug', $page_slug);

            if (!empty($page_cache_time))
                $this->pages->set_page($id_page, 'page_cache_time', $page_cache_time);

            $ext['action']          = 'reload';
            $ext['reload_delay']    = '500';
            ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
        }

        public function remove_page()
        {
            // Controlamos que el usuario tenga permiso para administrar paginas
                check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

            $id_page = $this->input->post('id_page');

            // Controlamos que el usuario tenga permiso de gestionar esta pagina.
                check_permissions('pages', $id_page, FALSE, TRUE);

            $result = $this->pages->del_page($id_page);
                
            if ($result == TRUE) 
            {
                $ext['action']          = 'reload';
                $ext['reload_delay']    = '500';
                ajax_response('success', $this->lang->line('pages_removed_succesfully'), '1', '1', $ext);
            }

            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

        public function save_custom_code()
        {
            $id_page            = $this->input->post('id_page');
            $page_custom_css    = $this->input->post('page_custom_css');
            $page_custom_js     = $this->input->post('page_custom_js');

            $this->pages->set_page($id_page, 'page_custom_css', $page_custom_css);
            $this->pages->set_page($id_page, 'page_custom_js', $page_custom_js);

            ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1');
        }

    // ESTA FUNCION GENERA UN MODAL PARA ELEGIR PAGINAS.
    // 
        public function get_page_picker()
        {
            $data['cfg_item']   = $this->input->post('cfg');
            $data['selected']   = $this->input->post('selecteds');
            $data['max_items']  = (int)$this->input->post('max');

            $ext['composer']    = $this->load->view('nawglobe/components/gtp_page_picker', $data, TRUE);
            echo ajax_response('success', 'Se puede seleccionar...', 1, 0, $ext);
        }
        public function get_page_picker_feed()
        {
            $selecteds = $this->input->post('selecteds');
            $selecteds = explode(',', $selecteds);

            $pages = $this->pages->get_pages(FALSE,FALSE,FALSE,500);

            // Las formateamos en JSON.
            $json_body = array();

            foreach ($pages as $key => $page) 
            {
                $id_parent_page = ($page['id_parent_page'] == 0) ? '#' : $page['id_parent_page'];
                $add_page['id']     = $page['id_page'];
                $add_page['parent'] = $id_parent_page;
                $add_page['text']   = $page['page_title'];
                $add_page['state']['selected']   = (in_array($page['id_page'], $selecteds));

                $json_body[] = $add_page;
            }

            $ext['feed']    = $json_body;
            ajax_response('success', 'Se puede seleccionar...', 1, 0, $ext);
        }

    // CAMBIOS EN LAS VERSIONES

        public function publish_version()
        {
            $version    = $this->input->post('version');
            $id_page    = $this->input->post('page');
            
            // Controlamos que el usuario tenga permiso para crear paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

            // Controlamos que el usuario tenga permiso de gestionar esta pagina.
            check_permissions('pages', $id_page, FALSE, TRUE);

            $result = $this->pages->publish_version($id_page, $version);

            if ($result == TRUE) 
            {
                $ext['action']          = 'reload';
                $ext['reload_delay']    = '500';
                ajax_response('success', $this->lang->line('pages_v_published_succesfully'), '1', '1', $ext);
            }
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }
        public function remove_version()
        {
            $version    = $this->input->post('version');
            $id_page    = $this->input->post('page');
            
            // Controlamos que el usuario tenga permiso para crear paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

            // Controlamos que el usuario tenga permiso de gestionar esta pagina.
            check_permissions('pages', $id_page, FALSE, TRUE);


            // Obtenemos la estructura de componentes.
            $page['id_page']        = $id_page;
            $page['page_version']   = $version;
            $components             = $this->components->get_components($page, FALSE, 0, 500);

            $result = $this->pages->del_version($id_page, $version);
            
            if ($result == TRUE)
            {
                // Eliminamos todos los componentes, y sus configuraciones.
                foreach ($components as $key => $component)
                {
                    $this->components->del_component($component['id_component']);
                    $this->configurations->clean_configurations('component', $component['id_component']);
                } 

                $ext['action']          = 'reload';
                $ext['reload_delay']    = '500';
                ajax_response('success', $this->lang->line('pages_v_removed_succesfully'), '1', '1', $ext);
            }
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }
        public function create_version()
        {
            $id_page        = $this->input->post('id_page');
            $from_version   = $this->input->post('from_version');
            $version_comment= $this->input->post('version_comment');
            
            // Controlamos que el usuario tenga permiso para crear paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

            // Controlamos que el usuario tenga permiso de gestionar esta pagina.
            check_permissions('pages', $id_page, FALSE, TRUE);

            $id_page_version = $this->pages->new_version($id_page, $version_comment);
            $version         = $this->pages->get_version($id_page_version);

            if ($version !== FALSE) 
            {

                if ($from_version !== 0) 
                {
                    // Obtenemos los componentes de la pagina de origen
                    $page['id_page']        = $id_page;
                    $page['page_version']   = $from_version;
                    $components             = $this->components->get_components($page, FALSE, 0, 500);

                    // Los duplicamos.
                    foreach ($components as $key => $component)
                    {
                        // Obtenemos los contenidos del componente original
                        $contents = $this->contents->get_contents(array('id_component' => $component['id_component']));

                        // Creamos el componente duplicado
                        $id_component = $this->components->new_component($id_page, $version['version_number'], FALSE, $component['th_component']);

                        // Clonamos la configuracion del componente inicial al duplicado.
                        $this->configurations->clone_configuration('component', $component['id_component'], $id_component);

                        // Duplicamos los contenidos del componente original, en el componente duplicado..
                        foreach ($contents as $content_key => $content) 
                            $this->contents->new_content($id_component, $content['content_name'], $content['content_value'], $content['content_lang']);
                    } 
                }

                $ext['action']          = 'reload';
                $ext['reload_delay']    = '500';
                ajax_response('success', $this->lang->line('pages_v_created_succesfully'), '1', '1', $ext);
            }
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

    // Esta funcion genera un modal para crear una version nueva
        public function get_version_creator()
        {
            $id_page    = $this->input->post('page');
            $versions   = $this->pages->get_versions($id_page);

            $data['id_page']    = $id_page;
            $data['versions']   = $versions;

            $ext['composer']    = $this->load->view('nawglobe/pages/pages/components/modal_create_version', $data, TRUE);
            echo ajax_response('success', 'Se puede seleccionar...', 1, 0, $ext);
        }
        public function get_previews()
        {
            $source = $this->input->post('source');
            $lang = $this->input->post('lang');
            $return = NULL;

            // Si tenemos elementos,
            if (strlen($source) > 0) 
            {
                $source = explode(',', $source);

                foreach ($source as $key => $item_page) {
                    $return .= "<span data-id='$item_page'>".text_preview(quitar_acentos(page_title($item_page, $lang), 12, '...')).'</span>';
                }
            }

            $ext['preview_data']    = $return;
            echo ajax_response('success', '...', 1, 0, $ext);
        }

    // Call para limpiar el cache de paginas

        public function clean_cache()
        {
            $cleaned = $this->output->clean_cache('nwg_page');
            
            $ext['action']          = 'reload';
            $ext['reload_delay']    = '500';
            echo ajax_response('success', 'Se vaciaron '.count($cleaned).' archivos de cache.', 1, 1, $ext);
        }
}
