<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_general extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        if(!$this->input->is_ajax_request()) 
			show_404();
    }

    // Aqui se guardan opciones de configuración. Este llamado se hace tanto desde gestorp como desde el editor de paginas.
	public function update_configs()
	{
		$configurations = $this->input->post();

		// Tomamos los datos del POST y separamos los que no corresponden a atributos de configuracion.
		$id_user 			= $this->session->userdata('id_user');
		$config_scope		= $configurations['config_scope']; 		unset($configurations['config_scope']);
		$config_reference	= $configurations['config_reference']; 	unset($configurations['config_reference']);
		$config_lang		= $configurations['config_lang']; 		unset($configurations['config_lang']);

		// Controlamos que tenga permiso para cambiar configuraciones.
		if ($config_scope == 'gestorp')
			check_permissions('gestorp', 'manage_configurations', FALSE, TRUE);

		// Controlamos que estas configuraciones a guardar, no pertenezcan a una entidad sobre la cual no tengo control.
		if ($config_scope == 'page')
			check_permissions('pages', $config_reference, FALSE, TRUE);

		$success = 0;
		$total = count($configurations);
		foreach ($configurations as $key => $value) {
			// Vemos si el elemento de configuracion es compuesto. En ese caso, esta delimitado por palotes.
			if (strpos($key, '|') === FALSE)
			{
				// Item de configuracion simple
				$success += (int)$this->configurations->set($config_scope, $key, $config_reference, (string)$value, $config_lang);
			}
			else
			{
				// Item de configuracion compuesto
				$cfg 		= explode('|', $key);
				$cfg_type 	= $cfg[0];
				$cfg_key 	= $cfg[1];

				// Obtenemos la configuracion que ya esta guardada
				$id_collection = $this->configurations->get($config_scope, $cfg_key, $config_reference, $config_lang);

				// Identificamos el tipo de configuracion.
				switch ($cfg_type) {
					case 'media_items':
						// Atualizamos la coleccion. Y registramos la coleccion como valor de configuracion para este item.
						$id_collection = $this->collections->upd_collection_items($id_collection, $value, 'media_items');
						$success += (int)$this->configurations->set($config_scope, $cfg_key, $config_reference, $id_collection, $config_lang);
						break;
					case 'page_items':
						// Atualizamos la coleccion. Y registramos la coleccion como valor de configuracion para este item.
						$id_collection = $this->collections->upd_collection_items($id_collection, $value, 'page_items');
						$success += (int)$this->configurations->set($config_scope, $cfg_key, $config_reference, $id_collection, $config_lang);
						break;
					default:
						# salteamos...
						break;
				}
			}
		}

		ajax_response('success', $this->lang->line('general_configurations_updated'), '1', '1');
	}

}
