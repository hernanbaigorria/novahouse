<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class S extends MX_Controller {

	function _output($output)
    {
    	if(!$this->input->is_ajax_request()) 
        	echo minify_html($output);
        else
        	echo $output;
    }
	
	public function index()
	{
		// Cargamos la configuración de sistema.
		$system = $this->configurations->get_configurations('system', 0, get_web_lang());
		
		// Si hay restricción de dominio para acceso publico.
		if (!empty($system['allow_domain'])) {
		    $allowed_domains = explode(',', $system['allow_domain']);
		    if (!in_array($_SERVER['HTTP_HOST'], $allowed_domains)) 
		    {
		    	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		    	exit();
		    }
		}

		// Si restringimos la invocación por iframe.
		if ($system['site_block_iframe']) {
			$this->output->set_header("Content-Security-Policy: frame-ancestors 'none'");
		}

		// Incializamos configs generales.
		$site_cache_time 		= (!empty($system['cache_time'])) 			? $system['cache_time'] 		: 0;
		$site_enable_profiler	= (!empty($system['enable_profiler'])) 		? $system['enable_profiler'] 	: FALSE;
		$site_enable_minify		= (!empty($system['enable_html_minify'])) 	? $system['enable_html_minify'] : FALSE;

		// Identificamos el primer segmento de  URI, y vemos si pertenece a alguna Seccion o pagina.
		$uri_section 	= $this->uri->segment(1);	// Seccion o Pagina...
		$uri_page 		= $this->uri->segment(2);	// Pagina ...
		$_render 		= FALSE;
		$_HTML 			= NULL;
		// Estamos accediendo al dominio sin especificar página?
		if (empty($uri_section)) 
		{
			// Cargamos configuracion general del sitio.
			$cfg = $this->configurations->get_configurations('site', 0, get_web_lang());
			
			if (isset($cfg['main_page']))
			{
				$homepage = $this->pages->get_page($cfg['main_page']);
				$_render = $homepage;
				$_HTML = render_page($homepage['id_page'], FALSE, FALSE, FALSE, TRUE);
			}
			else
				exit('No se ha configurado ninguna página principal.');
		}
		else
		{
			// Buscamos alguna seccion que cuente con ese slug.
				$filter['page_slug'] 	= $uri_section;
				$filter['is_external'] 	= FALSE;
				$result 				= $this->pages->get_pages($filter);
			
				// A esta altura, si encontramos algo lo renderizamos.
				if (count($result) > 0)
				{
					$_render = $page = $result[0];

					// vemos si es una seccion, o una pagina aislada.
					if ($page['is_section'])
					{
						// Si es una seccion, vemos si se especifico alguna subpagina de la seccion.
						if (empty($uri_page))
							$_HTML = render_page($page['id_page'], FALSE, FALSE, FALSE, TRUE);
						else
						{
							// Vemos si encontramos alguna subpagina con el slug indicado.
							$cond['id_parent_page']	= $page['id_page'];
							$cond['page_slug']		= $uri_page;
							$result 				= $this->pages->get_pages($cond);
							if (count($result) > 0) 
							{
								$_render = $subpage = $result[0];
								$_HTML = render_page($subpage['id_page'], FALSE, FALSE, FALSE, TRUE);
							}
							else
								show_404(); // Al no encontrar pagina, redirigimos a la home.
						}
					}
					else
						$_HTML = render_page($page['id_page'], FALSE, FALSE, FALSE, TRUE);
				}
				else
					show_404();
		}

		if ($site_enable_profiler == TRUE) {
			$this->output->enable_profiler(TRUE);
		}

		if ($site_enable_minify == TRUE) {
    		$_HTML = minify_html($_HTML);
		}

		$this->output->set_output($_HTML);

		if ($_render !== FALSE) 
		{
			$page_cache_time = $_render['page_cache_time'];

			if ($page_cache_time > 0) {
				$this->output->cache($page_cache_time);
			} 
			if ($page_cache_time == 0) {
				if ($site_cache_time > 0) {
					$this->output->cache($site_cache_time);
				}
			} 
		}
	}

	public function set_lang($lang)
	{
		$refferer = $_SERVER["HTTP_REFERER"];
		set_web_lang($lang);
		redirect($refferer);
	}

	public function contact()
	{
		$refferer = $_SERVER["HTTP_REFERER"];
		redirect($refferer);
	}

	public function ajax_contact()
	{
		$data['sender_name'] 	= $this->input->post(INP_CONTACT_NAME);
		$data['sender_mail'] 	= $this->input->post(INP_CONTACT_MAIL);
		$data['message'] 		= $this->input->post(INP_CONTACT_MESSAGE);
		$data['subject'] 		= $this->input->post(INP_CONTACT_SUBJECT);

		if (empty($data['subject'])) $data['subject'] = 'Web Contact';

		$message_body = $this->load->view('mails/1/web_message', $data, TRUE);

		$this->load->model('notifications');
	    $result = $this->notifications->send(CONTACT_TARGET_EMAILS, $data['subject'], $message_body, $on_debug = 'CONT');

		ajax_response('success', 'ok', '1', '1');
	}
	
	public function sitemap()
	{
		$cfg 	= $this->configurations->get_configurations('site', 0, get_web_lang());
		$cond 	= FALSE;
		
		if (isset($cfg['404_page']))
			$cond['pages.id_page!='] = $cfg['404_page'];

		$final_pages = array();
		$pages = $this->pages->get_active_pages($cond, FALSE, 0, 1000);
		foreach ($pages as $_kpage => $_page) {
			$url = page_uri($_page['id_page']);
			$final_pages[] = (strpos($url, base_url()) === FALSE) ? base_url($url) : $url;
		}

		$modules = get_modules(TRUE);
		foreach ($modules as $module) {
			$this->load->module($module);
			if (method_exists ( $this->$module , 'sitemap' )) {
				$urls = @$this->$module->sitemap();
				if (is_array($urls)) {
					foreach ($urls as $url) {
						$final_pages[] = $url;
					}
				}
			}
		}

		$data['pages'] = $final_pages;
		$this->load->view('nawglobe/pages/sitemap/main', $data);
	}
}
