<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nwgtesting extends MX_Controller {

    function __construct()
    {
        parent::__construct();

        // Si no estamos accediendo a la página de login, controlamos que la sesion este activa.
        $this->control_library->session_check();
    }

	public function mailing()
	{
		$content 	= "Sample Email";
		$email 		= $this->input->get('email');
		$email 		= (empty($empty)) ? $this->session->userdata('user_email') : $email;

		echo "Alias de despacho: ".EMAILS_SENDER_MAIL.'@'.EMAILS_DOMAIN.'<br>';
		echo "Receptor: ".$email.'<br>';
		echo "<br>";
		echo "Alias y receptor interno de notifiaciones se configura en /config.php <br>";
		echo "SMTP y otros se configuran en /application/config/email.php <br>";
		echo "Si /application/config/email.php no existe, se intenta enviar desde la cfg de mailing del servidor. <br>";
		echo "<br>";
		echo "LOG:<br>";
		echo "<br>";

		$this->load->library('email');

        $this->email->initialize();
        $this->email->from(EMAILS_SENDER_MAIL.'@'.EMAILS_DOMAIN, 'Lozada Viajes');
        $this->email->to($email);
        $this->email->subject(mb_convert_encoding('asunto', "UTF-8"));        
        $this->email->message(mb_convert_encoding($content, "UTF-8"));
        $this->email->set_mailtype("html");
        
        $this->email->send(); 
        echo $this->email->print_debugger();
	}

	public function phpinfo()
	{
		phpinfo();
	}

	public function fsk() 
	{
		$socket = fsockopen("ssl://smtp.gmail.com", 465, $errno, $errstr, 10);
		if(!$socket)
		{
			echo "ERROR: smtp.gmail.com 465 - $errstr ($errno)<br>\n";
		}
		else
		{
			echo "SUCCESS: smtp.gmail.com 465 - ok<br>\n";
		}

		$socket = fsockopen("smtp.gmail.com", 587, $errno, $errstr, 10);
		if(!$socket)
		{
			echo "ERROR: smtp.gmail.com 587 - $errstr ($errno)<br>\n";
		}
		else
		{
			echo "SUCCESS: smtp.gmail.com 587 - ok<br>\n";
		}
	}
}
