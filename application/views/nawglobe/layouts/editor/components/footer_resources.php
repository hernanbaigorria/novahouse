<?php # Requeridos para todo practicamente ?>
	<script src="<?php echo nwg_assets('plugins/jquery.min.js') ?>"></script>
	<script src="<?php echo nwg_assets('plugins/jquery-migrate.min.js') ?>"></script>
	<script src="<?php echo nwg_assets('plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
<?php # Fin de de requieridos para todo.. ?>

<?php # Requeridos para editor en linea ?>
	<script src="<?php echo nwg_assets('plugins/bootstrap-summernote/summernote.min.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/jquery-jeditable/jquery.jeditable.mini.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/jquery-jeditable/jquery.jeditable.wysiwyg.js') ?>" type="text/javascript"></script>
	<script src="<?php echo nwg_assets('plugins/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<?php # Fin de requieridos editor en linea.. ?>

	<?php #LIMPIAR ?>
		<?php # Requeridos para formularios ?>
			<script src="<?php echo nwg_assets('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/jquery.cokie.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/bootstrap-growl/jquery.bootstrap-growl.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/bootbox/bootbox.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/jquery-minicolors/jquery.minicolors.min.js') ?>" type="text/javascript"></script>
		<?php # FIN Requeridos para formularios. ?>

		<?php # Requeridos para usar los modales de configuracion y galeria. ?>
			<script src="<?php echo nwg_assets('plugins/cubeportfolio/js/jquery.cubeportfolio.min.js') ?>" type="text/javascript"></script>
			<script src="<?php echo nwg_assets('plugins/jstree/dist/jstree.min.js') ?>" type="text/javascript"></script>

			<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js') ?>"></script>
			<!-- The Templates plugin is included to render the upload/download listings -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/vendor/tmpl.min.js') ?>"></script>
			<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/vendor/load-image.min.js') ?>"></script>
			<!-- The Canvas to Blob plugin is included for image resizing functionality -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js') ?>"></script>
			<!-- blueimp Gallery script -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js') ?>"></script>
			<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.iframe-transport.js') ?>"></script>
			<!-- The basic File Upload plugin -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload.js') ?>"></script>
			<!-- The File Upload processing plugin -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-process.js') ?>"></script>
			<!-- The File Upload image preview & resize plugin -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-image.js') ?>"></script>
			<!-- The File Upload audio preview plugin -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-audio.js') ?>"></script>
			<!-- The File Upload video preview plugin -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-video.js') ?>"></script>
			<!-- The File Upload validation plugin -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-validate.js') ?>"></script>
			<!-- The File Upload user interface plugin -->
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/jquery.fileupload-ui.js') ?>"></script>
			<!-- The main application script -->
			<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
			<!--[if (gte IE 8)&(lt IE 10)]>
			<script src="<?php echo nwg_assets('plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js') ?>"></script>
			<![endif]-->
		<?php # Fin de requieridos en todos lados.. ?>
	<?php #LIMPIAR ?>

<?php # Requeridos para funcionar en gestorp ?>
	<script src="<?php echo nwg_assets('pages/js/ajax_engine.js') ?>"></script>
	<script src="<?php echo nwg_assets('pages/js/common.js') ?>"></script>
	<script src="<?php echo nwg_assets('layout/js/metronic.js') ?>"></script>
	<script src="<?php echo nwg_assets('pages/js/gtp_page_editor.js') ?>"></script>
	<script src="<?php echo nwg_assets('pages/js/gtp_general.js') ?>"></script>
	<script src="<?php echo nwg_assets('pages/js/gtp_media.js') ?>"></script>
<?php # Fin de requieridos para funcionar en gestorp.. ?>