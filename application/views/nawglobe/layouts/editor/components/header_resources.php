<link href="<?php echo nwg_assets('plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo nwg_assets('plugins/jquery-file-upload/css/jquery.fileupload.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo nwg_assets('plugins/jquery-file-upload/css/jquery.fileupload-ui.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo nwg_assets('plugins/jquery-minicolors/jquery.minicolors.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo nwg_assets('plugins/jstree/dist/themes/default/style.min.css') ?>" rel="stylesheet" type="text/css"/>

<link href="<?php echo nwg_assets('plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo nwg_assets('plugins/bootstrap-summernote/summernote.css') ?>" rel="stylesheet" type="text/css"/>

<link href="<?php echo nwg_assets('pages/css/common.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo nwg_assets('pages/css/gtp_page_editor.css') ?>" rel="stylesheet">
<link href="<?php echo nwg_assets('pages/css/gtp_media.css') ?>" rel="stylesheet">