<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>

	<title><?php echo GESTORP_NAME ?></title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?php echo HTTP_PROTOCOL ?>fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="<?php echo nwg_assets('plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo nwg_assets('plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo nwg_assets('plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo nwg_assets('plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL STYLES -->
	<?php if ($RESOURCES !== FALSE): ?>
	<!-- BEGIN PAGE LEVEL STYLES -->
		<?php foreach ($RESOURCES['header_css'] as $file => $resource): ?>
		<link href="<?php echo nwg_assets($resource) ?>" rel="stylesheet" type="text/css"/>
		<?php endforeach ?>
	<!-- END PAGE LEVEL STYLES -->
	<?php endif ?>
	<!-- END PAGE LEVEL SCRIPTS -->

	<!-- BEGIN THEME STYLES -->
	<link href="<?php echo nwg_assets('layout/css/components-md.css') ?>" rel="stylesheet" id="style_components" type="text/css"/>
	<link href="<?php echo nwg_assets('layout/css/plugins-md.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo nwg_assets('layout/css/layout_general.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo nwg_assets('layout/css/theme_general.css') ?>" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo nwg_assets('layout/css/custom.css') ?>" rel="stylesheet" type="text/css"/>
	<!-- END THEME STYLES -->
	<link rel="shortcut icon" href="<?php echo nwg_assets_theme('nawglobe/favicon.png') ?>"/>

	<script type="text/javascript">
		var base_url = "<?php echo base_url() ?>";
		var static_url = "<?php echo nwg_assets() ?>";
	</script>
</head>
<!-- END HEAD -->
