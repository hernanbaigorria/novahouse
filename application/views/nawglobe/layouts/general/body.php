<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner container">
		<?php if (@$COMPONENTS['bar_header'] == TRUE): ?>
		<?php $this->load->view('nawglobe/layouts/general/components/bar_header') ?>
		<?php endif ?>
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<?php if (@$COMPONENTS['bar_menu'] == TRUE): ?>
			<?php $this->load->view('nawglobe/layouts/general/components/bar_menu') ?>
			<?php endif ?>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
					<?php if (isset($PAGE_TITLE)) echo $PAGE_TITLE ?> <small><?php if (isset($PAGE_SUB_TITLE)) echo $PAGE_SUB_TITLE ?></small>&nbsp;
					<?php if (isset($PAGE_BUTTON)): ?>
						<a href="<?php echo @$PAGE_BUTTON['link'] ?>" class="btn pull-right <?php echo @$PAGE_BUTTON['class'] ?>">
						    <i class="fa <?php echo @$PAGE_BUTTON['icon'] ?>"></i>&nbsp;&nbsp; <?php echo @$PAGE_BUTTON['text'] ?>
						</a>
					<?php endif ?>
				</h3>
				<div class="page-bar">
					<?php if (@$COMPONENTS['bar_menu'] == TRUE): ?>
						<?php $this->load->view('nawglobe/layouts/general/components/breadcum') ?>
					<?php endif ?>
					<?php if (@$COMPONENTS['bar_toolbar'] == TRUE): ?>
						<div class="page-toolbar">
							<div class="btn-group pull-right">
								<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								Opciones <i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a href="#">Action</a>
									</li>
									<li>
										<a href="#">Another action</a>
									</li>
									<li>
										<a href="#">Something else here</a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="#">Separated link</a>
									</li>
								</ul>
							</div>
						</div>						
					<?php endif ?>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						 <?php echo @$CONTENT; ?>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>