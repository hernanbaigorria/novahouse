<?php $paginas = ceil($pagination_total_items / $pagination_per_page) ?>
<?php if ($paginas > 1): ?>
<div class="text-center">
	<ul class="pagination">
		<li class="<?php if ($pagination_current == 0) echo 'active' ?>">
			<a href="<?php if ($pagination_current == 0) echo 'javascript:;'; else echo current_url().generate_get_string('pagina', $pagination_current) ?>">
			<i class="fa fa-angle-left"></i>
			</a>
		</li>
		<?php

			$pagination_start 	= ($pagination_current - 5 > 0) ? ($pagination_current - 5) : 0;
			$pagination_end		= ($pagination_start + 10 < $paginas) ? $pagination_start + 10 : $paginas;

			for ($i=$pagination_start; $i < $pagination_end; $i++) { 
		 	?>

				<li class="<?php if ($pagination_current == $i) echo 'active' ?>">
					<a href="<?php echo current_url().generate_get_string('pagina', $i+1) ?>">
					<?php echo $i + 1 ?> 
					</a>
				</li>
		 	
		 	<?php  
			}
		 ?>
		<li class="<?php if ($pagination_current == ($paginas - 1)) echo 'active' ?>">
			<a href="<?php if ($pagination_current == ($paginas - 1)) echo 'javascript:;'; else echo current_url().generate_get_string('pagina', $pagination_current+2) ?>">
			<i class="fa fa-angle-right"></i>
			</a>
		</li>
	</ul>
</div>
<?php endif ?>