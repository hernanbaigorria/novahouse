<!-- /.modal -->
<div class="modal fade bs-modal-md" id="modal-page-picker" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 999999999999;">
	<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="gtp-modal-title modal-title"><?php echo $this->lang->line('general_pick_page') ?></h4>
				</div>
				<div class="modal-body">
					<div class="innerB">
						<input class="form-control gtp-form-control" id="page_tree_filter" placeholder="<?php echo $this->lang->line('general_filter'); ?>">
					</div>
					<div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
						<div class="row">
							<div class="col-md-12">
								<div id="pages_tree" data-selecteds="<?php echo $selected ?>"></div>
							</div>
						</div>
					</div>	
				</div>
				<div class="modal-footer">
				<form id="frm-page-picker">
					<input type="hidden" id="max_items" name="max_items" value="<?php echo $max_items ?>">
					<input type="hidden" id="cfg_item" name="cfg_item"  value="<?php echo $cfg_item ?>">

					<button type="button" data-dismiss="modal" class="gbtn gbtn-default"><span class="md-click-circle" style="height: 65px; width: 65px; top: -14.5px; left: 3.51563px;"></span><?php echo $this->lang->line('general_cancel') ?></button>
					<button type="submit" class="gbtn gbtn-success"><?php echo $this->lang->line('general_done') ?></button>
				</form>
				</div>
			</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

