<div class="page_collection_preview" data-source="<?php echo $reference ?>" data-text=" <?php echo $this->lang->line('general_pages_selected') ?>" data-lang="<?php echo $lang ?>">
	<?php foreach ($collection as $key => $item_page): ?>
	<span data-id="<?php echo $item_page ?>"><?php echo text_preview(quitar_acentos(page_title($item_page, $lang)), 12, '...') ?></span>
	<?php endforeach ?>
</div>