<div class="table-responsive">
	<table class="table table-striped table-bordered table-advance table-hover table-vertical-align margin-none">
		<thead>
			<tr>
				<th>
					Backup
				</th>
				<th style="width: 150px;">
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($files as $file): ?>
				<tr>
					<td>
						<?php echo $file ?>
					</td>
					<td>
						<a href="/uploads/backups/<?php echo $file ?>">Descargar</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>