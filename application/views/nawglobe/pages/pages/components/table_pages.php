<?php $pages = array_values($pages)?>
<?php if (count($pages) > 0): ?>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-advance table-hover table-vertical-align margin-none">
		<thead>
			<tr>
				<th>
					<?php if (isset($parent_slug)): ?>
						<?php echo $this->lang->line('pages_subpages') ?>
					<?php else: ?>
						<?php echo $this->lang->line('pages_page') ?>
					<?php endif ?>
				</th>
				<?php if (!$pages[0]['is_external']): ?>
				<th class="col-md-1 text-center">
					<?php echo $this->lang->line('pages_status') ?>
				</th>
				<?php endif ?>
				<th>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($pages as $key => $page): ?>
			<?php if (check_permissions('pages', $page['id_page'])): ?>
				<tr>
					<td>
						<strong><?php echo $page['page_title'] ?></strong>
						<br>
						<?php if ($pages[0]['is_external']): ?>
							<?php echo $page['page_slug'] ?>
						<?php else: ?>
							<?php if ($pages[0]['is_section']): ?>
								<a target="_blank" href="<?php echo base_url().$page['page_slug'] ?>"><?php echo base_url().$page['page_slug'] ?></a>
							<?php else: ?>
								<a target="_blank" href="<?php echo base_url().@$parent_slug.$page['page_slug'] ?>"><?php echo base_url().@$parent_slug.$page['page_slug'] ?></a>
							<?php endif ?>
						<?php endif ?>
					</td>
					<?php if (!$page['is_external']): ?>
					<td class="col-md-1 text-center">
						<?php if ($page['published_version'] == FALSE): ?>
							<?php echo $this->lang->line('pages_status_draft') ?>
						<?php else: ?>
							<?php echo $this->lang->line('pages_status_published') ?>
						<?php endif ?>
					</td>
					<?php endif ?>
					<td class="col-md-3 text-right">
						<?php if ($page['published_version'] !== FALSE): ?>
						<a class="btn btn-xs green" target="_blank" href="<?php echo base_url().GESTORP_MANAGER.'/page_edit' ?>?page=<?php echo $page['id_page']; ?>&version=<?php echo $page['published_version']; ?>">
							<?php echo $this->lang->line('general_live_edit') ?>
						</a>
						<?php endif ?>

						<a class="btn btn-xs default red-stripe" href="?action=edit&id=<?php echo $page['id_page']; ?>">
							<?php echo $this->lang->line('general_edit') ?>
						</a>

						<a class="btn btn-xs red red-stripe btn-remove-page" href="javascript:;" data-id_page="<?php echo $page['id_page'] ?>" data-confirmation="<?php echo $this->lang->line('pages_delete_sure') ?>">
							<i class="fa fa-trash-o"></i>
						</a>
					</td>
				</tr>
			<?php endif ?>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<?php else: ?>
	<h3 class="text-center innerB">
		<?php if (isset($parent_slug)): ?>
			<?php echo $this->lang->line('pages_no_sub_pages') ?>
		<?php else: ?>
			<?php echo $this->lang->line('pages_no_pages') ?>
		<?php endif ?>
	</h3>
<?php endif ?>