<div class="table-responsive innerB">
	<table class="table table-striped table-bordered table-advance table-hover table-vertical-align margin-none table-condensed">
		<tbody>
			<tr>
				<td>
					<strong><?php echo $page['page_title'] ?>  (<?php echo $this->lang->line('pages_section').' '.$this->lang->line('general_home_page') ?>)</strong>
					<br>
						<a target="_blank" href="<?php echo base_url().$page['page_slug'] ?>"><?php echo base_url().$page['page_slug'] ?></a>
				</td>
				<td class="col-md-1 text-center"  style="width: 180px;">
					<?php if ($page['published_version'] == FALSE): ?>
						<?php echo $this->lang->line('pages_status_draft') ?>
					<?php else: ?>
						<?php echo $this->lang->line('pages_status_published') ?>
					<?php endif ?>
				</td>
				<td class="col-md-3 text-right"  style="width: 140px;">
					<?php if (check_permissions('pages', $page['id_page'])): ?>
						<div class="btn-group">
							<?php if ($page['published_version'] !== FALSE): ?>
							<a class="btn btn-xs green" target="_blank" href="<?php echo base_url().GESTORP_MANAGER.'/page_edit' ?>?page=<?php echo $page['id_page']; ?>&version=<?php echo $page['published_version']; ?>">
								<i class="fa fa-pencil"></i>
							</a>
							<?php endif ?>

							<a class="btn btn-xs blue margin-none" href="?action=edit&id=<?php echo $page['id_page']; ?>">
								<i class="fa fa-cog"></i>
							</a>
						</div>
					<?php endif ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>