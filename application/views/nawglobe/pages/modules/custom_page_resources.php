<?php 

	// La variable $RESOURCES esta controlada, para agregarse en el header y footer.
	$RESOURCES = array(
			'header_css' => array(
				'plugins/select2o/css/select2.min.css',
				'plugins/select2o/css/select2-bootstrap.min.css',
				),
			'footer_js' => array(
				'plugins/select2o/js/select2.full.min.js'
				)
		);

	$COMPONENTS = array(
			'bar_header' => TRUE,
			'bar_menu' => TRUE,
			'bar_toolbar' => FALSE,
			'breadcum' => FALSE,
			'pre_footer' => TRUE,
			'footer' => TRUE
		);
 ?>