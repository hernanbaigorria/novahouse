<?php $__name = (!empty($media_name)) ? $media_name : $media_file_name; ?>
<div class="col-lg-2 col-md-4 col-sm-3 col-xs-3 media-item" title="<?php echo $__name ?>">
    <a href="?action=edit&id=<?php echo $id_media; ?>">
    	<?php if (strpos($media_type, 'video') !== FALSE): ?>
    		<span class="media-thumbnail">
				<video src="<?php echo THUMBNAILS_URL.$media_file_name ?>" style="width: 100%; height: 100%;"></video>
    		</span>
		<?php endif ?>
        <?php if (strpos($media_type, 'image') !== FALSE AND strpos($media_file_name, '.svg') == FALSE): ?>
        	<span class="media-thumbnail" style="background-image: url('<?php echo THUMBNAILS_URL.$media_file_name ?>')"></span>
		<?php endif ?>
        <?php if (strpos($media_type, 'pdf') !== FALSE): ?>
            <span class="media-thumbnail text-center" style="display: table;">
                <i class="fa fa-file-pdf-o" style="font-size: 30px; display: table-cell; vertical-align: middle;"></i>
            </span>
        <?php endif ?>
        <?php if (strpos($media_file_name, '.xls') !== FALSE): ?>
            <span class="media-thumbnail text-center" style="display: table;">
                <i class="fa fa-file-excel-o" style="font-size: 30px; display: table-cell; vertical-align: middle;"></i>
            </span>
        <?php endif ?>
        <?php if (strpos($media_file_name, '.doc') !== FALSE): ?>
            <span class="media-thumbnail text-center" style="display: table;">
                <i class="fa fa-file-word-o" style="font-size: 30px; display: table-cell; vertical-align: middle;"></i>
            </span>
        <?php endif ?>
        <?php if (strpos($media_file_name, '.svg') !== FALSE): ?>
            <span class="media-thumbnail" style="background-image: url('<?php echo THUMBNAILS_URL.$media_file_name ?>')"></span>
        <?php endif ?>
    </a>
    <div class="name uppercase text-center">
        <small><?php echo text_preview($__name, 43, '...') ?></small>
    </div>
</div>