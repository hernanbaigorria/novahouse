<div class="tabbable tabbable-custom tabbable-noborder ">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab-user-general" data-toggle="tab" aria-expanded="false">
				<?php echo $this->lang->line('general_account_configuration'); ?>
			</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab-user-general">
			<div class="row">
				<div class="col-md-12">
					<div class="tab-content">
						<div id="tab-user-info" class="tab-pane active">
							<form id="frm_create_account" role="form" action="javascript:;">
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_name'); ?></label>
									<input required name="user_name" type="text" placeholder="" class="form-control">
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_email'); ?></label>
									<input required name="user_email" type="email" placeholder="" class="form-control">
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_language'); ?></label>
									<select name="user_language" class="form-control">
										<option value="english">English</option>
										<option value="spanish">Spanish</option>
									</select>
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_new_password'); ?></label>
									<input name="user_password" type="password" class="form-control">
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('users_r_new_password'); ?></label>
									<input name="user_r_password" type="password" class="form-control">
								</div>
								<div class="margin-top-10">
									<a href="<?php echo base_url().GESTORP_MANAGER ?>/users" class="btn default">
										<?php echo $this->lang->line('general_cancel'); ?> 
									</a>
									<button type="submit" class="btn green">
										<?php echo $this->lang->line('general_save'); ?> 
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>