<!-- /.modal -->
<div class="modal fade bs-modal-md" id="modal-edit-component" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999999999;">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 class="gtp-modal-title modal-title"><?php echo $this->lang->line('pages_block_configuration') ?></h4>
			</div>
			<div class="modal-body">
				<?php echo generate_localized_configuration_form('component', $id_component, $lang); ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
