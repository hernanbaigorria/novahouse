<div class="row">
	<?php $system = $this->configurations->get_configurations('system', 0, get_web_lang()); ?>
	<?php if (!empty($system['cache_time']) AND $system['cache_time'] > 0): ?>
	<div class="col-md-4 ">
	    <!-- BEGIN Portlet PORTLET-->
	    <div class="portlet box blue-hoki">
	        <div class="portlet-title">
	            <div class="caption">
	                <i class="fa fa-files-o"></i>Cache </div>
	            <div class="actions">
	                <a href="javascript:;" class="btn btn-danger btn-sm btn-clean-cache">
	                    <i class="fa fa-trash-o"></i> Vaciar 
	                </a>
	            </div>
	        </div>
	        <div class="portlet-body">
                <strong>Estado</strong>
	            <ul class="list-unstyled">
	            	<li>Páginas cacheadas: <strong><?php echo $cache['file_count'] ?></strong></li>
	            	<li>Tamaño de cache: <strong><?php echo $cache['size_formated'] ?></strong></li>
	            </ul>
	        </div>
	    </div>
	    <!-- END Portlet PORTLET-->
	</div>
	<?php endif ?>
</div>