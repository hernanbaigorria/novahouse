<?php 

	// La variable $RESOURCES esta controlada, para agregarse en el header y footer.
	$RESOURCES = array(
			'header_css' => array(
				'pages/css/gtp_users.css',
				'plugins/bootstrap-fileinput/bootstrap-fileinput.css',
				),
			'footer_js' => array(
				'pages/js/gtp_users.js',
				'plugins/bootstrap-fileinput/bootstrap-fileinput.js',
				'plugins/jquery.blockui.min.js',
				)
		);

	$COMPONENTS = array(
			'bar_header' => TRUE,
			'bar_menu' => TRUE,
			'bar_toolbar' => FALSE,
			'breadcum' => FALSE,
			'pre_footer' => TRUE,
			'footer' => TRUE
		);
 ?>