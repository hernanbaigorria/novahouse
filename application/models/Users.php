<?php
	class Users extends CI_Model {
	public function __construct()
	{	}

    /**
     * Funciones utilizadas en Planeta 2.0
     */
        // Controla los datos para iniciar sesion, y retorna el usuario.
        public function login($user_mail, $user_password)
        {
            // Condiciones para login directo.
            $cond1['user_email']    = $user_mail;
            $cond1['user_password'] = $user_password;

            // Condiciones para login de cambio de clave.
            $cond2['user_email']            = $user_mail;
            $cond2['user_password_restore'] = $user_password;    // Viene encriptada

                      $this->db->where($cond1);
                      $this->db->where('user_deletion_date IS NULL', NULL, FALSE);
                      $this->db->or_group_start();
                      $this->db->or_where('user_email', $user_mail);
                      $this->db->where('user_password_restore', md5($user_password.date('Y-m-d')));  // Viene encriptada y se doble encripta.
                      $this->db->where('user_deletion_date IS NULL', NULL, FALSE);
                      $this->db->group_end();
            $result = $this->db->get('users');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                $result = $result[0];
                
                // Quitamos este dato de la respuesta para no almacenarla en la sesion.
                unset($result['usr_password']); 
                
                // Incrementamos los accesos
                $this->set_account($result['id_user'], 'user_last_login', date('Y-m-d H:i:s'));
                $this->set_account($result['id_user'], 'user_login_count', $result['user_login_count']+1);

                // Al ser correcto el inicio de sesion, retornamos los datos del usuario.
                return $result;
            }

            return FALSE;
        }

        // Crea una cuenta de usuario.
        public function new_account($user_name, $user_email, $user_language, $user_password)
        {
            $data['user_name']              = $user_name;
            $data['user_email']             = $user_email;
            $data['user_language']          = $user_language;
            $data['user_password']          = $user_password;
            $data['user_registration_date']  = date('Y-m-d H:i:s');

            $result = $this->db->insert('users', $data);

            if ($result == TRUE) 
                return $this->db->insert_id();
            else
                return FALSE; 
        }

        // Crea una cuenta de usuario.
        public function del_account($id_user)
        {
            $data['user_deletion_date'] = date('Y-m-d H:i:s');

                      $this->db->where('id_user', $id_user);
            $result = $this->db->update('users', $data);

            return $result;
        }

        // Genera el una co-contraseña para acceder el dia de hoy a la cuenta.
        public function gen_temporal_password($id_user = FALSE)
        {
            $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
            if (empty($id_user)) return FALSE;

            // Generamos un token personal.
            $tmp_password   = generate_string(5);
            // Generamos el hash de comparacion para ese token, valido por el dia de hoy.
            // Notese que se hace md5 en dos etapas. primero a la clave, y despues al md5 resultante concatenado con la fecha del dia.
            // Esto para poder hacer la comparacion, porque desde el modelo recibimos directamente el md5 de la clave, y no sabemos cual es la clave sin encriptar.
            $tmp_token      = md5($tmp_password).date('Y-m-d');
            // Seteamos el dato en la cuenta.
            $result         = $this->set_account($id_user, 'user_password_restore', md5($tmp_token));
            
            // Retornamos la clave para que se la notifiquen por email.
            if ($result != 0)
                return $tmp_password;

            return FALSE;
        }

        // Permite setear alguna propiedad de un usuario.
        public function set_account($id_user = FALSE, $filter_column = FALSE, $filter_value = FALSE)
        {
            $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
            if (empty($id_user)) return FALSE;
            
            $data = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $data[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $data[$filter_column] = $filter_value;

            $cond['id_user'] = $id_user;

                    $this->db->where($cond);
            return  $this->db->update('users', $data);
        }

        // Compureba que el email no se encuentre registrado.
        public function chk_email($email, $asked_by = FALSE)
        {
            // Permitimos no comparar contra nosotros mismos, o determinado usuario.
            if ($asked_by !== FALSE) 
                $data['id_user !='] = $asked_by;
            
            $data['user_email'] = $email;

            $this->db->where($data);
            $this->db->where('user_deletion_date IS NULL', NULL, FALSE);
            return (bool)($this->db->count_all_results('users') > 0);
        }

    // Gestion de la cuenta del usuario.

        public function get_user($id_user)
        {
            if ($id_user === FALSE) return FALSE;
            $cond['id_user'] = $id_user;

            $this->db->where($cond);
            $this->db->where('user_deletion_date IS NULL', NULL, FALSE);
            $result = $this->db->get('users');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0];
            }

            return FALSE;
        }

        // Obtiene los usuarios.
        public function get_users($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20)
        {
            // Procesamos filtros flexibles, por parametro.
            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $cond['users.'.$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $cond['users.'.$filter_column] = $filter_value;
                else
                    if ($filter_column !== FALSE)
                        $cond['users.id_user'] = $filter_column;

            // Construimos la query
            $this->db->select('SQL_CALC_FOUND_ROWS users.*', FALSE);
            $this->db->from('users');
            
            if (isset($cond))
            if (count($cond) > 0) 
                $this->db->where($cond);

            $this->db->where('users.user_deletion_date IS NULL', NULL, FALSE);
            $this->db->order_by('users.user_registration_date', 'DESC');

            if ($page !== FALSE) {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $result =   $this->db->get();
            $paginacion = $this->db->query('SELECT FOUND_ROWS() pages')->result_array();

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                foreach ($result as $key => $value) 
                {
                    $result[$key]['total_results'] = $paginacion[0]['pages'];
                }
                return $result;
            }

            return array();
        }

        // Activa la cuenta del usuario
        public function get_user_by($column = FALSE, $value = FALSE, $allow_multiple_results = FALSE)
        {
            if (empty($column) OR empty($value) OR $value == '1')
                return FALSE;
            else
                $cond[$column] = $value;

                      $this->db->select('users.*, IFNULL(users.user_picture, "default.jpg") user_picture');
                      $this->db->from('users');
                      $this->db->where($cond);
                      $this->db->where('users.user_deletion_date IS NULL', NULL, FALSE);
            $result = $this->db->get();

            if ($result->num_rows() > 0)
            {

                $result = $result->result_array();
                if (!$allow_multiple_results)
                    return $result[0];
                else
                    return $result;
            }

            return FALSE;
        }

        // Actualiza el email de usuario
        public function upd_user_email($id_user = FALSE, $email)
        {
            $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
            if (empty($id_user) or empty($email)) return FALSE;

            // Si el email ya esta utilizado, cancelamos la operacion.
            if ($this->account->chk_email($email, $id_user)) return FALSE;
            
            // Seteamos el nuevo email.
            $result = (bool)(int)$this->set_account($id_user, 'usr_email', $email);

            // Comprobamos si estamos reescribiendo el email.
            if ($email == $this->session->userdata('usr_email'))
                // Si lo reescribimos, es logico que set_account devuelva 0 porque no se updateo ninguna fila.
                return !$result;
            else
                // Si es un email nuevo, entonces deberiamos tener un 1 en set_account
                return $result;
        }

        // Retorna la foto de perfil del usuario
        // Return: Simple Array on success, False on error
        public function get_profile_picture($id_user = FALSE)
        {
            $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
            if (empty($id_user)) return FALSE;

            $where['id_user']   = $id_user;

                      $this->db->select('users.*');
                      $this->db->from('users');
                      $this->db->where($where);
            $result = $this->db->get();

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0]['user_picture'];
            }

            return FALSE;
        }

        // Elimina la foto de perfil de un usuario
        // Return: True on succes, False on error.
        public function del_profile_picture($id_user = FALSE, $replace_with = NULL)
        {
            $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
            if (empty($id_user)) return FALSE;

            $current_pic = $this->get_profile_picture($id_user);

            // Si esta la foto por defecto, la dejamos.
            if ($current_pic == NULL) return TRUE;

            // Eliminamos tanto el original, como las copias.
            if (!empty($current_pic))
            {
                @unlink(PATH_STATIC.PATH_PROFILEPICTURES_ORIGINAL.$current_pic);
            }

            // Le ponemos de perfil la foto por defecto.
            return $this->set_user_profile_picture($id_user, NULL);
        }

        // Establece una foto de perfil
        // Return: True on success, False on error.
        public function set_user_profile_picture($id_user = FALSE, $new_picture)
        {
            $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
            if (empty($id_user)) return FALSE;

            $data['user_picture'] = $new_picture;

            $where['id_user']   = $id_user;

                      $this->db->where($where);
            $result = $this->db->update('users', $data);

            return $result;
        }


}


