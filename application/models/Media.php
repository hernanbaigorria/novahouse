<?php
class Media extends CI_Model 
{
    
    public function __construct() 
    {

    }

    // Gestion de archivos

        public function new_file($media_type, $media_name, $media_file_name, $media_file_size)
        {
            $data['media_type'] = $media_type;
            $data['media_name'] = $media_name;
            $data['media_file_name'] = $media_file_name;
            $data['media_file_size'] = $media_file_size;

            return $this->db->insert('media', $data);
        }

        public function upd_file($id_media = FALSE, $media_type, $media_name, $media_file_name, $media_file_size)
        {
            if ($id_media === FALSE) return FALSE;

            $data['media_type'] = $media_type;
            $data['media_name'] = $media_name;
            $data['media_file_name'] = $media_file_name;
            $data['media_file_size'] = $media_file_size;

            $cond['id_media'] = $id_media;

            $this->db->where($cond);
            return $this->db->update('media', $data);
        }

        public function get_file($id_media)
        {
            if ($id_media === FALSE) return FALSE;
            $cond['id_media'] = $id_media;

            $this->db->where($cond);
            $result = $this->db->get('media');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0];
            }

            return FALSE;
        }

        public function del_file($id_media)
        {
            if ($id_media === FALSE) return FALSE;

            // Tenemos los datos a mano para luego borrar el archivo.
            $media = $this->get_file($id_media);

            // Borrado Logico
                      $this->db->where('id_media', $id_media);
            $result = $this->db->delete('media');
            $result = $this->db->affected_rows();

            if ($result > 0) {
                // Borrado Fisico
                @unlink(PATH_UPLOADS.$media['media_file_name']);
                @unlink(PATH_THUMBNAILS.$media['media_file_name']);
                return TRUE;
            }
            return FALSE;
        }

        public function get_media($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $media_type = 'all', $selection_sort = NULL, $name_filter = NULL, $group_filter = NULL)
        {
            $cond = array();

            // Creamos una tabla temporal con los elemengos marcados.
                if (!empty($selection_sort))
                    $this->db->select('*, (CASE WHEN id_media IN ('.$selection_sort.') THEN 1 ELSE 0 END) as media_selected', FALSE);
                else
                    $this->db->select('*, (CASE WHEN 1 = 1 THEN 0 END) as media_selected', FALSE);
                
                $this->db->from('media');
                $gtp_media_query = $this->db->get_compiled_select();
                $TMP_MEDIA = 'U_'.generate_string(5);
                $this->db->query("CREATE TEMPORARY TABLE $TMP_MEDIA ($gtp_media_query)", FALSE);


            $this->db->order_by('media_selected', 'DESC');
            $this->db->select('*');
            $this->db->from($TMP_MEDIA);

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $cond[$filter_column] = $filter_value;
                else
                    if ($filter_column !== FALSE)
                        $cond['id_media'] = $filter_column;

            if (count($cond) > 0)
                $this->db->where($cond);

            if (!empty($name_filter)) {
                $this->db->group_start();
                $this->db->like('media_name', $name_filter, 'both');
                $this->db->or_like('media_file_name', $name_filter, 'both');
                $this->db->or_where('media_selected', '1');
                $this->db->group_end();
            }

            if(!empty($group_filter) AND $group_filter != '0') {
                $this->db->group_start();
                $this->db->where('media_group', $group_filter);
                $this->db->or_where('media_selected', '1');
                $this->db->group_end();
            }

            // Filtro para cuando se pide solo videos.
            if ($media_type == 'video')
                $this->db->like('media_type', 'video', 'after');

            // Filtro para solo imagenes.
            if ($media_type == 'image')
                $this->db->like('media_type', 'image', 'after');

            // Filtro para solo imagenes.
            if ($media_type == 'pdf')
                $this->db->like('media_type', 'pdf', 'both');

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $this->db->order_by('media_selected', 'DESC');
            $this->db->order_by('upload_date', 'DESC');
            $result = $this->db->get();
             #echo $this->db->last_query();
            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                #debugger($result, 1);
                return $result;
            }

            return array();
        }

        public function set_file($id_media, $filter_column = FALSE, $filter_value = FALSE)
        {
            $data = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $data[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $data[$filter_column] = $filter_value;

            $cond['id_media'] = $id_media;

            $this->db->where($cond);
            $result = $this->db->update('media', $data);

            return $this->db->affected_rows();
        }

    // Gestion de grupos

        public function new_group($group_name)
        {
            $data['group_name'] = $group_name;
            $data['id_user']    = $this->session->userdata('id_user');
            $data['creation_date'] = date('Y-m-d H:i:s');

            $result = $this->db->insert('media_groups', $data);

            if ($result == TRUE) 
                return $this->db->insert_id();
            else
                return FALSE; 
        }

        public function get_groups()
        {

                      $this->db->order_by('id_group', 'DESC');
            $result = $this->db->get('media_groups');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result;
            }

            return array();
        }

        public function get_group($id_group)
        {
            if ($id_group === FALSE) return FALSE;
            $cond['id_group'] = $id_group;

            $this->db->where($cond);
            $result = $this->db->get('media_groups');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0];
            }

            return FALSE;
        }

        public function del_group($id_group)
        {
            if ($id_group === FALSE) return FALSE;

            // Tenemos los datos a mano para luego borrar el archivo.
            $media = $this->get_file($id_group);

            // Borrado Logico
                      $this->db->where('id_group', $id_group);
            $result = $this->db->delete('media_groups');
            $result = $this->db->affected_rows();

            if ($result > 0)
                return TRUE;
            return FALSE;
        }

        public function set_group($id_group, $filter_column = FALSE, $filter_value = FALSE)
        {
            $data = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $data[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $data[$filter_column] = $filter_value;

            $cond['id_group'] = $id_group;

            $this->db->where($cond);
            $result = $this->db->update('media_groups', $data);

            return $this->db->affected_rows();
        }

}