<?php
class Pages extends CI_Model 
{
    
    public function __construct() 
    {

    }

    // GESTION DE PAGINAS

        public function new_page($is_section, $is_external, $id_parent_page = FALSE, $page_title, $page_slug, $page_order, $th_layout)
        {
            $data['is_section']     = $is_section;
            $data['is_external']    = $is_external;
            $data['id_parent_page'] = $id_parent_page;
            $data['page_title']     = $page_title;
            $data['page_slug']      = $page_slug;
            $data['page_order']     = $page_order;
            $data['th_layout']      = $th_layout;
            
            $data['page_cache_time']        = 0;
            $data['page_custom_css']        = NULL;
            $data['page_custom_js']         = NULL;

            $result = $this->db->insert('pages', $data);

            if ($result == TRUE) 
                return $this->db->insert_id();
            else
                return FALSE; 
        }

        public function get_page($id_page, $with_parent = FALSE)
        {
            if ($id_page === FALSE) return FALSE;
            $cond['id_page'] = $id_page;

            $this->db->where($cond);
            $result = $this->db->get('pages');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                $result = $result[0];

                if ($result['id_parent_page'] !== 0 AND $with_parent == TRUE)
                    $result['parent_page'] = $this->get_page($result['id_parent_page']);

                $result['published_version'] = $this->get_published_version($id_page);
                return $result;
            }

            return FALSE;
        }

        public function del_page($id_page)
        {
            if ($id_page === FALSE) return FALSE;
            $cond['id_page'] = $id_page;

            // Eliminamos todas las versiones de esta pagina.
            $this->del_all_versions($id_page);

            // Limpiamos las configuraciones guardadas para esta pagina.
            $this->configurations->clean_configurations('page', $id_page);
            $this->permissions->clean_permissions('pages', $id_page);

            $this->db->where($cond);
            $result = $this->db->delete('pages');

            return $this->db->affected_rows();
        }

        public function get_pages($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20)
        {
            $cond = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $cond[$filter_column] = $filter_value;
                else
                    if ($filter_column !== FALSE)
                        $cond['id_page'] = $filter_column;

            if (count($cond) > 0)
                $this->db->where($cond);

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $result = $this->db->get('pages');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                foreach ($result as $key => $page) {
                    $result[$key]['published_version'] = $this->get_published_version($page['id_page']);
                }
                return $result;
            }

            return array();
        }

        public function set_page($id_page, $filter_column = FALSE, $filter_value = FALSE)
        {
            $data = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $data[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $data[$filter_column] = $filter_value;

            $cond['id_page'] = $id_page;

            $this->db->where($cond);
            $result = $this->db->update('pages', $data);

            return $this->db->affected_rows();
        }

        public function get_active_pages($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20)
        {
            $cond = array();
            $this->db->select('pages.*, page_versions.version_number, page_versions.published', FALSE);
            $this->db->from('pages');
            $this->db->join('page_versions', 'page_versions.id_page = pages.id_page AND page_versions.published = 1');

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $cond[$filter_column] = $filter_value;
                else
                    if ($filter_column !== FALSE)
                        $cond['id_page'] = $filter_column;

            if (count($cond) > 0)
                $this->db->where($cond);

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $result = $this->db->get();

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                foreach ($result as $key => $page) {
                    $result[$key]['published_version'] = $this->get_published_version($page['id_page']);
                }
                return $result;
            }

            return array();
        }

    // CONSULTAS DE LISTADO

        // Retorna solo las paginas que son secciones.
        public function get_sections($with_chids = FALSE)
        {
            $conditions['is_section'] = TRUE;
            $sections = $this->get_pages($conditions, FALSE, FALSE, 500);

            if ($with_chids == TRUE)
            {
                foreach ($sections as $key => $section) 
                {
                    $ch_conditions['id_parent_page'] = $section['id_page'];
                    $sections[$key]['childs'] = $this->get_pages($ch_conditions, FALSE, FALSE, 500);
                }
            }

            return $sections;
        }

        // Retorna solo las paginas externas.
        public function get_externals()
        {
            $conditions['is_external']      = TRUE;
            $conditions['id_parent_page']   = 0;
            $pages = $this->get_pages($conditions, FALSE, FALSE, 500);
            return $pages;
        }

        // Retorna solo las paginas aisladas.
        public function get_lonley_pages()
        {
            $conditions['is_section']       = FALSE;
            $conditions['is_external']      = FALSE;
            $conditions['id_parent_page']   = 0;
            $pages = $this->get_pages($conditions, FALSE, FALSE, 500);
            return $pages;
        }

        // Retorna las paginas sobre las cuales se pueden fijar permisos. Que serian todas menos las externas.
        public function get_regulable_pages()
        {
            $pages = $this->get_pages(FALSE, FALSE, FALSE, 500);
            $response = array();
            foreach ($pages as $key => $page) 
            {
                $id                      = $page['id_page'];
                $response[$id]['value']  = DEFAULT_PERMISSION_PAGE_VALUE;
                $response[$id]['label']  = $page['page_title'];
            }

            return $response;
        }

    // VERSIONADO DE PAGINAS

        // Genera una nueva version para una pagina. Se puede especificar desde cual version va a generarse, en dicho caso, clonara esa version.
        public function new_version($id_page, $version_comment = NULL)
        {
            $version['id_page']         = $id_page;
            $version['version_number']  = $this->get_next_version_number($id_page);
            $version['version_comment'] = $version_comment;
            $version['date_creation']   = date('Y-m-d H:i:s');
            $version['version_last_modification']   = date('Y-m-d H:i:s');
            $version['version_last_modifier']       = $this->session->userdata('id_user');
            $version['date_creation']   = date('Y-m-d H:i:s');
            $version['published']       = FALSE;

            $result     = $this->db->insert('page_versions', $version);

            if ($result == TRUE)
            {
                $result_id  = $this->db->insert_id();
                return $result_id;
            }
            return FALSE;
        }

        public function get_version($id_page_version = FALSE, $id_page = FALSE, $version_number = FALSE)
        {
            if ($id_page_version === FALSE AND $id_page === FALSE AND $version_number === FALSE) return FALSE;

            if ($id_page_version !== FALSE)
                $cond['id_page_version'] = $id_page_version;
            else
            {
                $cond['id_page']            = $id_page;
                $cond['version_number']     = $version_number;
            }


            $this->db->where($cond);
            $result = $this->db->get('page_versions');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                $result = $result[0];
                return $result;
            }

            return FALSE;
        }

        public function get_versions($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order = FALSE)
        {
            $cond = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $cond[$filter_column] = $filter_value;
                else
                    if ($filter_column !== FALSE)
                        $cond['id_page'] = $filter_column;

            if (count($cond) > 0)
                $this->db->where($cond);

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $this->db->order_by('version_number', 'DESC');
            $result = $this->db->get('page_versions');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result;
            }

            return array();
        }

        public function del_version($id_page = FALSE, $version_number = FALSE)
        {
            if ($id_page === FALSE) return FALSE;
            if ($version_number === FALSE) return FALSE;
            

            // Eliminamos completamente los componentes pertenecientes a esa pagina.
            $components_filter['id_page']           = $id_page;
            $components_filter['page_version']      = $version_number;
            $componentes = $this->components->get_components($components_filter, FALSE, 0, 500);
            foreach ($componentes as $key => $component) 
                $this->components->del_component($component['id_component']);

            // Eliminamos la version.
            $cond['id_page']        = $id_page;
            $cond['version_number'] = $version_number;
            $this->db->where($cond);
            $result = $this->db->delete('page_versions');

            return $this->db->affected_rows();
        }

        public function del_all_versions($id_page)
        {
            // Obtenemos todas las versiones de la pagina.
            $versiones = $this->get_versions($id_page);

            // Las eliminamos completamente una a una.
            foreach ($versiones as $key => $version) 
                $this->del_version($id_page, $version['version_number']);
        }

        // Obtiene el siguiente numero de version, para la pagina especificada.
        public function get_next_version_number($id_page)
        {
            $this->db->select_max('version_number');
            $this->db->where('id_page', $id_page);

            $result =   $this->db->get('page_versions');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0]['version_number'] + 1;
            }

            return 1;
        }

        // Actualizamos la fecha de modificación de la pagina.
        public function publish_version($id_page, $version_number)
        {
            // Desactivamos todas las versiones.
            $data['published'] = FALSE;
            $this->db->where('id_page', $id_page);
            $this->db->update('page_versions', $data);

            // Activamos la version solicitada
            $data['published'] = TRUE;
            $this->db->where('id_page', $id_page);
            $this->db->where('version_number', $version_number);
            return $this->db->update('page_versions', $data);
        }

        // Actualizamos la fecha de ultima publicación de la pagina.
        public function get_published_version($id_page)
        {
            $this->db->where('id_page', $id_page);
            $this->db->where('published', TRUE);
            $result =   $this->db->get('page_versions');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0]['version_number'];
            }

            return FALSE;
        }

    // HELPERS

        // Verifica que la url no este utilizada. Devuelve TRUE si se puede uusar, False si no.
        public function chk_slug($page_slug, $id_parent_page = FALSE, $id_page = FALSE, $type = FALSE)
        {
            // Si se especifica contra que pagina no se debe comparar...
            if ($id_parent_page !== FALSE)
                $conditions['id_page!=']  = $id_page;

            // Si tiene un parent, controlamos que la uri no se repita en el entorno del parent.
            if ($id_parent_page !== FALSE)
                $conditions['id_parent_page']  = $id_parent_page;

            if ($type !== FALSE)
                $conditions['is_section'] = ($type == 'section') ? '1' : '0';

            $conditions['page_slug'] = $page_slug;

            $result = $this->get_pages($conditions);

            return !(count($result) >= 1);
        }

        // Renderiza una pagina.
        public function render_page($id_page, $lang = FALSE, $version = FALSE, $editable = FALSE, $return = FALSE)
        {
            // Verificamos que la pagina exista en el sistema.
            $page          = $this->get_page($id_page,  TRUE);
            if ($page == FALSE)     return FALSE;               // Si la pagina no existe, devolvemos FALSE.
            if ($lang == FALSE)     $lang = get_web_lang();     // Si no especificaron lenguaje, mostramos el lenguaje web seleccionado.

            // Cargamos la configuración de la pagina y del sitio, y la combinamos priorizando la de la pagina.
            $site_cfg = $this->configurations->get_configurations('site', 0, $lang);
            $page_cfg = $this->configurations->get_configurations('page', $id_page, $lang);

            if ($page['id_parent_page'] != 0)
            {
                $parent_cfg = $this->configurations->get_configurations('page', $page['id_parent_page'], $lang);
                $page_cfg   = $this->configurations->get_configurations('page', $id_page, $lang);
                $data['PAGE_CFG']   = cfg_merge($site_cfg, cfg_merge($parent_cfg, $page_cfg));
            }
            else
            {
                $page_cfg = $this->configurations->get_configurations('page', $id_page, $lang);
                $data['PAGE_CFG']   = cfg_merge($site_cfg, $page_cfg);
            }

            $data['LANGS']      = $this->config->item('available_languages');
            $data['page']       = $page; // <- se deberia deprecar.
            $data['PAGE']       = $page;
            
            // Si se nos especifica una version, sobrescribimos la version seteada como publica.
            if ($version !== FALSE)
                $data['page']['published_version'] = $version;

            // Si no hay version, devolvemos false.
            if ($data['page']['published_version'] == FALSE)
                return FALSE;

            // Si es una subpagina, cargamos el layout de la pagina parent.
            $layout  = ($page['id_parent_page'] != 0) ? $page['parent_page']['th_layout'] : $page['th_layout'];

            $data['VERSION'] = $version;
            $data['ID_PAGE'] = $id_page;

            return $this->layouts->render_layout($layout, $data, $lang, $editable, $return);
        }

        public function search($term = NULL, $page = 0, $items_per_page = 20)
        {
            $term = $this->db->escape_like_str($term);
            
            if (strlen($term) < 3)
                return array();

            // Buscamos los componentes que tienen contenido deseado.
                $this->db->select("page_components_content.*, page_components.id_page, page_components.page_version, MATCH(page_components_content.content_value) AGAINST('$term' IN BOOLEAN MODE) as score", FALSE);
                $this->db->from('page_components_content');
                $this->db->join('page_components', "page_components_content.id_component = page_components.id_component", 'left');
                $this->db->having('score >', 0);
                $search_in_components_content = $this->db->get_compiled_select();
            
                $TMP_TABLE_COMPONENTS = 'U_'.generate_string(10);
                $this->db->query("CREATE TEMPORARY TABLE $TMP_TABLE_COMPONENTS ($search_in_components_content)", FALSE);

            // Buscamos paginas cuyo titulo machee los terminos, filtrado tambien por versiones activas.
                $this->db->select("pages.id_page, page_versions.version_number as page_version, '1' AS score", FALSE);
                $this->db->from('pages');
                $this->db->join('page_versions', "pages.id_page = page_versions.id_page", 'left');
                $this->db->where("page_versions.published", 1);
                $this->db->like('pages.page_title', $term, 'both');
                $results_by_meta = $this->db->get_compiled_select();
                $this->db->query("INSERT INTO $TMP_TABLE_COMPONENTS (id_page, page_version, score) $results_by_meta", FALSE);

            // Buscamos los elementos de cfg que cumplan los terminos
                $this->db->select("configurations.config_reference AS id_page, page_versions.version_number as page_version, MATCH(configurations.config_value) AGAINST('$term' IN BOOLEAN MODE) AS score", FALSE);
                $this->db->from('configurations');
                $this->db->join('page_versions', "configurations.config_reference = page_versions.id_page AND configurations.config_scope = 'page'", 'left');
                $this->db->where("page_versions.published", 1);
                $this->db->having('score >', 0);
                $results_by_cfg = $this->db->get_compiled_select();
                $this->db->query("INSERT INTO $TMP_TABLE_COMPONENTS (id_page, page_version, score) $results_by_cfg", FALSE);

            // Creamos la tabla provisoria con las paginas de los resultdos de contenido, ya filtrado por versiones activas.
                $this->db->select("$TMP_TABLE_COMPONENTS.id_page, $TMP_TABLE_COMPONENTS.page_version, SUM(score) AS score, page_versions.published", FALSE);
                $this->db->from($TMP_TABLE_COMPONENTS);
                $this->db->join('page_versions', "$TMP_TABLE_COMPONENTS.id_page = page_versions.id_page AND $TMP_TABLE_COMPONENTS.page_version = page_versions.version_number AND page_versions.published = 1", 'left');
                $this->db->where("page_versions.published", 1);
                $this->db->group_by("id_page");
                $this->db->order_by("score", "DESC");
                $results_by_content = $this->db->get_compiled_select();
                
                $TMP_TABLE_PROVISORY = 'P_'.generate_string(10);
                $this->db->query("CREATE TEMPORARY TABLE $TMP_TABLE_PROVISORY ($results_by_content)", FALSE);

            $result = $this->db->get($TMP_TABLE_PROVISORY);

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result;
            }

            return array();

        }
}