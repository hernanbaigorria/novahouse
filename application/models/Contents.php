<?php
class Contents extends CI_Model 
{
    
    public function __construct() 
    {

    }


    public function new_content($id_component, $content_name, $content_value, $content_lang)
    {
        $data['id_component'] = $id_component;
        $data['content_name'] = $content_name;
        $data['content_value'] = $content_value;
        $data['content_lang'] = $content_lang;

        $result = $this->db->insert('page_components_content', $data);

        if ($result == TRUE)
            return $this->db->insert_id();

        return FALSE;
    }

    public function upd_content($id_content = FALSE, $id_component, $content_name, $content_value, $content_lang)
    {
        if ($id_content === FALSE) return FALSE;

        $data['id_component'] = $id_component;
        $data['content_name'] = $content_name;
        $data['content_value'] = $content_value;
        $data['content_lang'] = $content_lang;

        $cond['id_content'] = $id_content;

        $this->db->where($cond);
        return $this->db->update('page_components_content', $data);
    }

    public function get_content($id_content)
    {
        if ($id_content === FALSE) return FALSE;
        $cond['id_content'] = $id_content;

        $this->db->where($cond);
        $result = $this->db->get('page_components_content');

        if ($result->num_rows() > 0)
        {
            $result = $result->result_array();
            return $result[0];
        }

        return FALSE;
    }

    public function del_content($id_content)
    {
        if ($id_content === FALSE) return FALSE;
        $cond['id_content'] = $id_content;

        $this->db->where($cond);
        $result = $this->db->delete('page_components_content');

        return $this->db->affected_rows();
    }

    public function get_contents($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20)
    {
        $cond = array();

        if (is_array($filter_column))
        {
            foreach ($filter_column as $key => $value) {
                $cond[$key] = $value;
            }
        }
        else
            if ($filter_column !== FALSE AND $filter_value !== FALSE)
                $cond[$filter_column] = $filter_value;
            else
                if ($filter_column !== FALSE)
                    $cond['id_content'] = $filter_column;

        if (count($cond) > 0)
            $this->db->where($cond);

        if ($page !== FALSE)
        {
            $offset = $page*$page_items;
            $this->db->limit($page_items, $offset);
        }
        
        $result = $this->db->get('page_components_content');

        if ($result->num_rows() > 0)
        {
            $result = $result->result_array();
            return $result;
        }

        return array();
    }

    public function set_content($id_content, $filter_column = FALSE, $filter_value = FALSE)
    {
        $data = array();

        if (is_array($filter_column))
        {
            foreach ($filter_column as $key => $value) {
                $data[$key] = $value;
            }
        }
        else
            if ($filter_column !== FALSE AND $filter_value !== FALSE)
                $data[$filter_column] = $filter_value;

        $cond['id_content'] = $id_content;

        $this->db->where($cond);
        $result = $this->db->update('page_components_content', $data);

        return $this->db->affected_rows();
    }

    // HELPERS

    // RENDERIZADO

        public function render_content($name, $tag, $id_component, $lang = FALSE, $editable = FALSE, $type = 'text')
        {
            // Obtenemos el contenido
                $cond['content_name']   = $name;
                $cond['id_component']   = $id_component;
                $cond['content_lang']   = $lang;
                $saved_content           = $this->get_contents($cond);

                // En caso de que no exista, debemos crearlo.
                if (count($saved_content) > 0) 
                {
                    $saved_content  = $saved_content[0];
                    $id_content     = $saved_content['id_content'];
                    $content        = $saved_content['content_value'];
                }
                else
                {
                    // Al no existir este contenido, lo creamos en blanco.
                    $id_content = $this->new_content($id_component, $name, '', $lang);
                    $content    = NULL;

                    if ($id_content == FALSE)
                        echo "{error}";
                }

            switch ($type) {
                case 'html':
                    if ($editable) 
                        echo "<$tag class='gtp_editable_html' id='$id_content'>$content</$tag>";
                    else
                        echo "<$tag>$content</$tag>";
                    break;

                default:
                    if ($editable)
                        echo "<$tag class='gtp_editable_text' id='$id_content'>$content</$tag>";
                    else
                        echo "<$tag>$content</$tag>";
                    break;
            }
        }

        public function return_content($name, $id_component, $lang = FALSE)
        {
            // Obtenemos el contenido
                $cond['content_name']   = $name;
                $cond['id_component']   = $id_component;
                $cond['content_lang']   = $lang;
                $saved_content           = $this->get_contents($cond);

                // En caso de que no exista, debemos crearlo.
                if (count($saved_content) > 0) 
                {
                    $saved_content  = $saved_content[0];
                    $id_content     = $saved_content['id_content'];
                    $content        = $saved_content['content_value'];
                }
                else
                {
                    // Al no existir este contenido, lo creamos en blanco.
                    $id_content = $this->new_content($id_component, $name, '', $lang);
                    $content    = NULL;

                    if ($id_content == FALSE)
                        return "{error}";
                }

            return $content;
        }
}