<?php
class Collections extends CI_Model 
{
    
    public function __construct() 
    {

    }

    // Devuelve la info de una coleccion.
    public function get_collection($id_collection, $collection_type = FALSE)
    {
        if ($id_collection === FALSE) return FALSE;
        $cond['id_collection'] = $id_collection;

        if ($collection_type !== FALSE)
            $cond['collection_type'] = $collection_type;

        $this->db->where($cond);
        $result = $this->db->get('collections');

        if ($result->num_rows() > 0)
        {
            $result = $result->result_array();
            return $result[0];
        }

        return FALSE;
    }

    // Devuelve la los items de una coleccion.
    public function get_collection_items($id_collection, $as_array = FALSE)
    {
        if ($id_collection === FALSE) return array();
        $cond['id_collection'] = $id_collection;

        $this->db->where($cond);
        $this->db->order_by('item_order', 'asc');
        $result = $this->db->get('collection_items');

        if ($result->num_rows() > 0)
        {
            $result = $result->result_array();

            if ($as_array)
                return $result;

            $values = array_column($result, 'item_value');
            $result = implode(',', $values);
            return $result;
        }

        if ($as_array)
            return array();
        return NULL;
    }

    // Crea una nueva coleccion.
    public function new_collection($collection_type)
    {
        $data['collection_type']    = $collection_type;
        $data['creation_date']      = date('Y-m-d H:i:s');

        $result = $this->db->insert('collections', $data);

        if ($result == TRUE) 
            return $this->db->insert_id();
        else
            return FALSE; 
    }

    public function del_collection($id_collection)
    {
        if ($id_collection === FALSE) return FALSE;
        $cond['id_collection'] = $id_collection;

        $this->db->where($cond);
        $result = $this->db->delete('collections');

        return (bool)$this->db->affected_rows();
    }

    // Agrega un item a una coleccion.
    public function add_item($id_collection = FALSE, $item = FALSE, $order = FALSE)
    {
        if ($id_collection == FALSE OR (int)$item == 0)
            return FALSE;

        if (is_array($item))
            $item = json_encode($item);

        $item_data['id_collection'] = $id_collection;
        $item_data['item_value']    = $item;
        $item_data['item_order']    = (int)$order;

        $result = $this->db->insert('collection_items', $item_data);

        if ($result == TRUE) 
            return $this->db->insert_id();
        else
            return FALSE; 
    }

    // Actualiza los items una coleccion. Devuelve el ID de la coleccion.
    public function upd_collection_items($id_collection, $items, $collection_type = FALSE)
    {
        $collection = $this->get_collection($id_collection, $collection_type);

        // Creamos la coleccion en caso de que no exista.
        if ($collection == FALSE)
            if ($collection_type == FALSE) 
                show_error("Colecciones: Se intento crear una colección automaticamente, pero no tiene el tipo definido.", 500, 'Error de GestorP');
            else
                $id_collection = $this->new_collection($collection_type);

        // Limpiamos la coleccion.
            $this->empty_collection($id_collection);

        // Guardamos los items en la coleccion.
            if (!is_array($items))
                $items = explode(',', $items);

            foreach ($items as $key => $item) 
                $this->add_item($id_collection, $item, $key);

        return $id_collection;
    }

    // Elimina todas las referencias que haya al item proporcionado.
    public function clean_references($collection_type = FALSE, $item_value = FALSE)
    {
        if ($collection_type == FALSE OR $item_value == FALSE) return FALSE;
        
        $this->db->select('collection_items.id_item');
        $this->db->from('collection_items');
        $this->db->join('collections', 'collection_items.id_collection = collections.id_collection', 'left');
        $this->db->where('collections.collection_type', $collection_type);
        $this->db->where('collection_items.item_value', $item_value);
        $this->db->group_by("collection_items.id_item");
        $query_items = $this->db->get_compiled_select();

        $TMP_TABLE_DELETE_ITEMS = 'U_'.generate_string(10);
        $this->db->query("CREATE TEMPORARY TABLE $TMP_TABLE_DELETE_ITEMS ($query_items)", FALSE);

                    $this->db->where("id_item IN (SELECT * FROM $TMP_TABLE_DELETE_ITEMS)", NULL, FALSE);
        $result =   $this->db->delete('collection_items');

        $this->output->enable_profiler(TRUE);

        return $result;
    }

    // UTILS

        // Vacia una coleccion.
        public function empty_collection($id_collection)
        {
            if ($id_collection === FALSE) return FALSE;
            $cond['id_collection'] = $id_collection;

            $this->db->where($cond);
            $result = $this->db->delete('collection_items');

            return $result;
        }

        // Clona una coleccion, en una nueva coleccion.
        public function clone_colection($id_collection_source)
        {
            // Obtenemos la info de la coleccion de origen.
            $source_collection = $this->get_collection($id_collection_source);

            // Creamos una coleccion nueva.
            $id_new_collection = $this->new_collection($source_collection['collection_type']);

            // Agregamos los items de la coleccion de orgien, en la nueva.
            $source_items      = $this->get_collection_items($source_collection['id_collection'], TRUE);

            foreach ($source_items as $key => $item) 
                $this->add_item($id_new_collection, $item['item_value']);

            return $id_new_collection;
        }

}