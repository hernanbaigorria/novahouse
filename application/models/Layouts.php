<?php
class Layouts extends CI_Model 
{

    public function __construct() {}

    // Retorna un listado de los layouts posibles.
    public function get_layouts()
    {
        $layouts_path     = PATH_THEME_CODE.'layouts';

        // Tenemos un layout por carpeta, asique buscamos las carpetas.
        $layouts = glob($layouts_path . '/*' , GLOB_ONLYDIR);

        // Limpiamos las rutas, porque glob nos da la ruta completa del directorio.
        foreach ($layouts as $key => $layout) 
        {
            $layout         = explode('/', $layout);
            if (isset($layout[1]))
                $layouts[$key]  = $layout[count($layout) - 1];   // tomamos solo el nombre del directorio.
        }

        return $layouts;
    }

    // Renderiza un layout.
    public function render_layout($layout_name, $data = NULL, $lang = FALSE, $editable = FALSE, $return = FALSE)
    {
        // Verificamos que exista el layout en el tema.
            $layout_folder      = PATH_THEME_CODE.'layouts'.DIRECTORY_SEPARATOR.$layout_name;
            $layout_view        = THEME_LOAD_PATH.'layouts'.DIRECTORY_SEPARATOR.$layout_name.DIRECTORY_SEPARATOR.'layout';
            
            if (!is_dir($layout_folder)) {
                show_error("Layouts: No se encuentra el layout: ".$layout_name);
            }

        // Verificamos que los datos provistos, contengan la información minima.
            if (!isset($data['page']) OR !isset($data['PAGE_CFG'])) {
                show_error("Layouts: No se especifico pagina o configuracion, a mostrar en layout: ".$layout_name);
            }

        // Si no se especifico un lenguaje, cargamos el web por defecto.
            if ($lang == FALSE) $lang = get_web_lang();
       
        // Información disponible desde aqui en adelnate
        // $data['PAGE_CFG']    con la configuracion del layout.
        // $data['page']        con los atributos de la pagina a dibujar. (internamente los layout renderizan los componentes a partir de esto)
        // $data['VERSION']     con el numero de version de la pagina que se muestra

        // Estas contienen los recursos necesarios para que el editor funcione.
        $data['EDITOR_HEADER']  = ($editable == FALSE) ? NULL : $this->load->view('nawglobe/layouts/editor/components/header_resources', NULL, TRUE);
        $data['EDITOR_FOOTER']  = ($editable == FALSE) ? NULL : $this->load->view('nawglobe/layouts/editor/components/footer_resources', NULL, TRUE);
        
        // Estas se utilizan para los renderizados, disponibles siempre.
        $data['LANG']           = $lang;
        $data['EDITABLE']       = ($editable == FALSE) ? FALSE : TRUE;

        return $this->load->front($layout_view, $data, $return);
    }
}
