<?php 

$lang['login_admin_access']				= 'Accedo Administradores';
$lang['login_password'] 				= 'Clave';
$lang['login_continue'] 				= 'Continuar';
$lang['login_restore_password']			= '¿Olvidaste la clave?';
$lang['login_write_email_password']		= 'Escriba su email y clave';
$lang['login_forget_password']			= '¿Olvidaste la clave?';
$lang['login_enter_email_address']		= 'Escriba su dirección de email y le enviaremos una clave temporal.';



// Mensajes de notificaicon
$lang['login_incorrect_email_or_p']		= 'Email o Clave incorrectos.';
$lang['login_user_no_exist'] 			= 'Aparentemente no estas registrado en el sistema.';
$lang['login_temp_password_problem']	= 'Ocurrio un error generando tu clave temporal.';
$lang['login_temp_password_ok'] 		= 'Listo, hemos enviado una clave temporal a tu email.';
