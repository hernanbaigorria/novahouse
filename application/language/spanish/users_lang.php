<?php 

$lang['users_users'] 					= 'Usuarios';
$lang['users_user'] 					= 'Usuario';
$lang['users_edit_user'] 				= 'Editar usuario';
$lang['users_create_user'] 				= 'Crear usuario';
$lang['users_avatar'] 					= 'Avatar';
$lang['users_registration_date']		= 'Fecha de registro';
$lang['users_last_login']				= 'Último acceso';
$lang['users_login_count']				= 'Cantidad de accesos';
$lang['users_new_user'] 				= 'Nuevo usuario';
$lang['users_name'] 					= 'Nombre';
$lang['users_email'] 					= 'Email';
$lang['users_current_password']			= 'Clave actual';
$lang['users_new_password']				= 'Nueva clave';
$lang['users_r_new_password']			= 'Repita la nueva clave';
$lang['users_language']					= 'Idioma';
$lang['users_permissions']				= 'Permisos';


$lang['users_update_profile_picture']	= 'Actualizar imagen de perfíl';
$lang['users_change_picture']			= 'Cambiar imagen';
$lang['users_pick_another']				= 'Seleccionar otra';
$lang['users_upload_picture']			= 'Subir imagen';


// Mensajes de notificaicon
$lang['users_created_succesfully'] 		= 'Usuario creado correctamente';
$lang['users_removed_succesfully'] 		= 'Usuario eliminado correctamente';
$lang['users_delete_sure'] 				= '¿Estas seguro?';
