<?php 

// Traducciones de permisos (Rotulos que se muestran durante el seteo)
	$lang['permission_type_gestorp'] 		= 'Del Gestor';
	$lang['permission_type_pages'] 			= 'De Páginas';
	$lang['permission_type_modules'] 		= 'De Módulos';
	
	$lang['permission_manage_users'] 		= 'Adminstrar usuarios';
	$lang['permission_manage_permissions'] 	= 'Administrar permisos de usuarios';
	$lang['permission_manage_configurations'] = 'Cambiar las configuraciones del sitio';
	$lang['permission_manage_modules'] 		= 'Ver el menu de Módulos';
	$lang['permission_manage_media'] 		= 'Ver la libreria multimedia';
	$lang['permission_manage_pages'] 		= 'Administrar páginas';
	$lang['permission_manage_create_pages'] = 'Crear páginas nuevas';
	
	$lang['permission_upload_files'] 	= 'Subir archivos';
	$lang['permission_edit_files'] 		= 'Editar archivos subidos';
	$lang['permission_replace_files'] 	= 'Reemplazar archivos';
	$lang['permission_remove_files'] 	= 'Eliminar archivos';
