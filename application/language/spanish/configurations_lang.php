<?php 

// Traducciones de elementos de configuracion.
	$lang['configurations_allow_domain'] 			= 'Allowed domains (comma separated, no spaces)';
	$lang['configurations_cache_time'] 				= 'Cache time (in seconds)';
	$lang['configurations_enable_profiler'] 		= 'Enable Profiler <strong class="text-danger">(DO NOT ENABLE THIS IN PRODUCTION!)</strong>';
	$lang['configurations_enable_html_minify'] 		= 'Minify HTML (may use a bit more CPU, so recomended when cache is enabled)';
	$lang['configurations_site_block_iframe'] 		= 'Prevent iframe embedding';
	#$lang['configurations_NAME'] 			= 'TRANSLATION';

// Agregamos las traducciones del tema.
	$CUR_LANGUAGE = 'spanish';
	@include(PATH_THEME_CODE.DIRECTORY_SEPARATOR.'translations'.DIRECTORY_SEPARATOR.$CUR_LANGUAGE.'.php');