<?php 

$lang['pages_pages'] 					= 'Pages';
$lang['pages_pages_simples'] 			= 'Simple Pages';
$lang['pages_pages_grouped'] 			= 'Grouped Pages';
$lang['pages_pages_external'] 			= 'Links';
$lang['pages_subpages'] 				= 'Sub-Pages';
$lang['pages_page'] 					= 'Page';
$lang['pages_sections'] 				= 'Sections';
$lang['pages_section'] 					= 'Section';
$lang['pages_section_page'] 			= 'Page of Section...';
$lang['pages_external_pages'] 			= 'External Links';
$lang['pages_edit_page'] 				= 'Edit Page';
$lang['pages_edit_version'] 			= 'Edit Version';
$lang['pages_create_page'] 				= 'Create new page';
$lang['pages_create_subpage'] 			= 'Create a sub-page';
$lang['pages_delete_section'] 			= 'Delete this section';
$lang['pages_new_page'] 				= 'New Page';
$lang['pages_back'] 					= 'Back to pages';
$lang['pages_page_basics'] 				= 'Settings';
$lang['pages_page_content'] 			= 'Page Content';
$lang['pages_page_versions'] 			= 'Versions';
$lang['pages_page_settings'] 			= 'Page Settings';
$lang['pages_page_name'] 				= 'Internal Name';
$lang['pages_page_slug'] 				= 'Public URI';
$lang['pages_page_parent_section']		= 'Parent Section';
$lang['pages_page_layout'] 				= 'Layout';
$lang['pages_page_version'] 			= 'Version';
$lang['pages_page_new_version'] 		= 'New Version';
$lang['pages_page_verion_creation_date']= 'Created';
$lang['pages_page_version_comment']		= 'Comment';
$lang['pages_spage_uri'] 				= '/subpages-uri';
$lang['pages_wich_page_type'] 			= 'which type of page would be?';
$lang['pages_descr_pages']	 			= 'Who doesnt belong to a section, for example a home page, a contact page, or a special landing page could be a "simple page"';
$lang['pages_descr_section']			= 'Wich can contain another pages inside.';
$lang['pages_descr_section_pages'] 		= 'Wich belong to the section, and inherits its design.';
$lang['pages_descr_external'] 			= 'A reference for an external site.';
$lang['pages_create_from'] 				= 'Create a new version from: ';

$lang['pages_status'] 					= 'Status';
$lang['pages_status_online'] 			= 'Published';
$lang['pages_status_draft'] 			= 'Draft';
$lang['pages_status_to_update']			= 'Changes Unpublished';
$lang['pages_status_published']			= 'Published';

$lang['pages_add_block'] 				= 'Add Block';
$lang['pages_add_module'] 				= 'Add Module';
$lang['pages_page_configuration']		= 'Page Options';
$lang['pages_block_configuration']		= 'Block Configuration';

$lang['pages_page_cache']				= 'Cache';
$lang['pages_page_cache_disabled']		= 'Disabled';
$lang['pages_page_cache_site']			= 'Site defined';
$lang['pages_page_cache_minutes']		= 'Minutes';
$lang['pages_page_cache_days']			= 'Days';
$lang['pages_page_custom']				= 'CSS/JS';
$lang['pages_page_custom_css']			= 'CSS';
$lang['pages_page_custom_js']			= 'JS';

$lang['pages_no_pages']					= 'No pages to show.';
$lang['pages_no_sub_pages']				= 'No sub-pages to show.';
$lang['pages_discard_version']			= 'Discard';
$lang['pages_publish_version']			= 'Publish';
$lang['pages_publish_version_editor']	= 'This version is not published. Click here to publish';
$lang['pages_empty_version']			= 'Empty Version';
$lang['pages_editing_live']				= 'Attention: You are currently editing a published version. Changes are live made.';


// Mensajes de notificaicon
$lang['pages_error_no_layout'] 			= 'Must select a layout.';
$lang['pages_error_short_slug'] 		= 'Slug is too short.';
$lang['pages_error_short_title'] 		= 'Title is too short.';
$lang['pages_error_no_parent'] 			= 'Must select a parent section.';
$lang['pages_error_uri_used'] 			= 'That URI is already used.';

$lang['pages_created_succesfully'] 		= 'Page created successfully';
$lang['pages_removed_succesfully'] 		= 'Page removed successfully';
$lang['pages_v_created_succesfully'] 	= 'Version created successfully';
$lang['pages_v_published_succesfully'] 	= 'Version published successfully';
$lang['pages_v_removed_succesfully'] 	= 'Version removed successfully';
$lang['pages_delete_sure'] 				= 'Are you sure?';
$lang['pages_publish_version_sure'] 	= 'Are you sure?';
$lang['pages_remove_version_sure'] 		= 'Are you sure?';
