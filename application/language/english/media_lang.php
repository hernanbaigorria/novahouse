<?php 

// Rotulos
$lang['media_media'] 					= 'Multimedia Files';
$lang['media_upload_new_items'] 		= 'Upload new Items';
$lang['media_select_files'] 			= 'Select Files to upload';
$lang['media_do_upload'] 				= 'Start Uploads';
$lang['media_no_more_items'] 			= 'there´s no more items.';
$lang['media_edit_item'] 				= 'Edit media item';
$lang['media_back_gallery']				= 'Back to gallery';
$lang['media_uploaded']					= 'Uploaded on';
$lang['media_size']						= 'File Size';
$lang['media_group']					= 'Group';
$lang['media_manage_group']				= 'Manage Groups';
$lang['media_all_groups']				= 'All Groups';
$lang['media_without_group']			= 'Without Group';
$lang['media_new_group']				= 'New Group';
$lang['media_edit_group']				= 'Edit Group';
$lang['media_items_in_group']			= '~ Items';

// Campos de fomularios
$lang['media_name']						= 'Media Name';
$lang['media_file_name']				= 'File Name';
$lang['media_public_link']				= 'Public Link';
$lang['media_magic_link']				= 'Short Permanent Link (Only to be used inside the site)';

// Mensajes de notificaicon
$lang['media_created_succesfully'] 		= 'Media created successfully';
$lang['media_group_created_succesfully']= 'Group created successfully';
$lang['media_removed_succesfully'] 		= 'Media file removed successfully';
$lang['media_group_removed_succesfully']= 'Media group removed successfully';
$lang['media_group_updated_succesfully']= 'Media group updated successfully';
$lang['media_delete_sure'] 				= 'Are you sure?';
$lang['media_replace_file'] 			= 'Replace File';
$lang['media_replace_file_type_err']    = 'can´t be replaced with a file of distinct type';
$lang['media_replace_file_help'] 		= 'You can replace this file keeping all the references. Just pick a new file. <br> Note: Replace with a file of same type, otherwise may break something on the web presentation.';
