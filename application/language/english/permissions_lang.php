<?php 

// Traducciones de permisos (Rotulos que se muestran durante el seteo)
	$lang['permission_type_gestorp'] 		= 'GestorP';
	$lang['permission_type_pages'] 			= 'Pages';
	$lang['permission_type_modules'] 		= 'Modules';
	
	$lang['permission_manage_users'] 		= 'Manage Users';
	$lang['permission_manage_permissions'] 	= 'Manage Users Permissions';
	$lang['permission_manage_configurations'] = 'Change Site Configurations';
	$lang['permission_manage_modules'] 		= 'View Modules Menu';
	$lang['permission_manage_media'] 		= 'View Media Files';
	$lang['permission_manage_pages'] 		= 'Manage Pages';
	$lang['permission_manage_create_pages'] = 'Create New Pages';
	
	$lang['permission_upload_files'] 	= 'Upload Files';
	$lang['permission_edit_files'] 		= 'Edit Files';
	$lang['permission_replace_files'] 	= 'Replace Files';
	$lang['permission_remove_files'] 	= 'Remove Files';
