<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$ADDED_CFG['allow_domain']['default']			= '';
	$ADDED_CFG['allow_domain']['multilang'] 		= FALSE;
	$ADDED_CFG['allow_domain']['permissions'] 		= FALSE;
	$ADDED_CFG['allow_domain']['type'] 				= 'text';
	
	$ADDED_CFG['cache_time']['default']				= '-1';
	$ADDED_CFG['cache_time']['multilang'] 			= FALSE;
	$ADDED_CFG['cache_time']['permissions'] 		= FALSE;
	$ADDED_CFG['cache_time']['type'] 				= 'text';

	$ADDED_CFG['enable_profiler']['default'] 		= '1';
	$ADDED_CFG['enable_profiler']['multilang'] 	 	= FALSE;
	$ADDED_CFG['enable_profiler']['permissions'] 	= FALSE;
	$ADDED_CFG['enable_profiler']['type'] 			= 'checkbox';

	$ADDED_CFG['enable_html_minify']['default'] 		= '1';
	$ADDED_CFG['enable_html_minify']['multilang'] 	 	= FALSE;
	$ADDED_CFG['enable_html_minify']['permissions'] 	= FALSE;
	$ADDED_CFG['enable_html_minify']['type'] 			= 'checkbox';

	$ADDED_CFG['site_block_iframe']['default'] 			= '1';
	$ADDED_CFG['site_block_iframe']['multilang'] 	 	= FALSE;
	$ADDED_CFG['site_block_iframe']['permissions'] 		= FALSE;
	$ADDED_CFG['site_block_iframe']['type'] 			= 'checkbox';
	