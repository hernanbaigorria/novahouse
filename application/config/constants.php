<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Definiciones para el tema.
|--------------------------------------------------------------------------
|
| Se cargan desde el archivo theme-config.php
|
*/
	@include(FCPATH.'config.php');
	
	// Las validamos.
	
	if (!defined('GESTORP_THEME')) 
		exit('La configuración inicial no esta definida.');

/*
|--------------------------------------------------------------------------
| GestorP Configuration Constants
|--------------------------------------------------------------------------
|
| 
|
*/
define('DEFAULT_LANGUAGE', 	'english');

// FULL PATHS
define('PATH_WEB', 			FCPATH.'web'.DIRECTORY_SEPARATOR);
define('PATH_THEME', 		PATH_WEB.GESTORP_THEME.DIRECTORY_SEPARATOR);
define('PATH_THEME_CODE', 	PATH_THEME.'code'.DIRECTORY_SEPARATOR);
define('PATH_THEME_ASSETS', PATH_THEME.'assets'.DIRECTORY_SEPARATOR);
define('PATH_UPLOADS', 		FCPATH.'uploads'.DIRECTORY_SEPARATOR);
define('PATH_THUMBNAILS', 	PATH_UPLOADS.DIRECTORY_SEPARATOR.'thumbnail'.DIRECTORY_SEPARATOR);

// PATHS para LOAD * _ci_load ya tiene la FCPATH completa en su codigo, por eso le pasamos una base reducida.
define('THEME_LOAD_PATH', 	GESTORP_THEME.DIRECTORY_SEPARATOR.'code'.DIRECTORY_SEPARATOR);

// URLS * Para user en views y layouts.
define('THEME_ASSETS_URL', 	'/web/'.GESTORP_THEME.'/assets/');
define('UPLOADS_URL', 		'/uploads/');
define('THUMBNAILS_URL', 	'/uploads/thumbnail/');

// Configuracion del gestor.
define('GESTORP_MANAGER', 					'manager');							// nombre del controller de administracion.
define('GESTORP_PUBLIC', 					'S');							// nombre del controller contenido publico.
define('SYSTEM_PATH_MEDIA', 				'./assets/gp-uploads/media/');		// ruta donde se suben los archivos.
define('PATH_STATIC', 						'./assets');						// ruta donde se encuentran los archivos estaticos.
define('PATH_PROFILEPICTURES_ORIGINAL', 	'/gp-uploads/users/');				// ruta donde se suben las imagenes de perfil.
define('URL_SWITCH_LANG', 					GESTORP_PUBLIC.'/set_lang/');				// ruta donde se suben las imagenes de perfil.

// Parametros de formularios de contacto
define('URL_CONTACT_FORM', 					GESTORP_PUBLIC.'/contact/');				// ruta donde se suben las imagenes de perfil.
define('URL_CONTACT_AJAX_FORM', 			GESTORP_PUBLIC.'/ajax_contact/');				// ruta donde se suben las imagenes de perfil.
define('CONTACT_FORM_CLASS', 				'contact_form');			
define('INP_CONTACT_NAME', 					'contact_name');
define('INP_CONTACT_MAIL', 					'contact_mail');
define('INP_CONTACT_MESSAGE', 				'contact_message');
define('INP_CONTACT_SUBJECT', 				'contact_subject');

// Constantes de paginacion
define('PAGINATION_USER_LIST', 10);												// Cantidad a mostrar por pagina.
define('PAGINATION_MEDIA_LIST', 12);											// Cantidad a mostrar por pagina.

define('NAWGLOBE_NAME', 	'Nawglobe');												// Cantidad a mostrar por pagina.
define('NAWGLOBE_VERSION', 	'1.6');												// Cantidad a mostrar por pagina.

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
