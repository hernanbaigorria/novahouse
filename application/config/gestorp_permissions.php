<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  Configuracion de Permisos
 *
 *  $config[permissions][PERMISION_GROUP][PERMISSION_ITEM] = PERMISSION_DEFAULT_VALUE
 *
 *  Asegurarse que el PERMISSION_ITEM tenga su linea de traduccion en general_lang, de lo
 *  contrario en las pantallas de configuracion se vera el rotulo en blanco.
 *
 *  PERMISSION_DEFAULT_VALUE puede ser TRUE o FALSE
 */

// Permiisos para menus y pantallas

	$config['permissions']['gestorp']['manage_users'] 			= TRUE;
	$config['permissions']['gestorp']['manage_permissions'] 	= TRUE;
	$config['permissions']['gestorp']['manage_modules'] 		= TRUE;
	$config['permissions']['gestorp']['manage_pages'] 			= TRUE;
	$config['permissions']['gestorp']['manage_create_pages'] 	= TRUE;
	$config['permissions']['gestorp']['manage_configurations'] 	= TRUE;

	$config['permissions']['gestorp']['manage_media'] 	= TRUE;
	$config['permissions']['gestorp']['upload_files'] 	= TRUE;
	$config['permissions']['gestorp']['edit_files'] 	= TRUE;
	$config['permissions']['gestorp']['remove_files'] 	= TRUE;
	$config['permissions']['gestorp']['replace_files'] 	= TRUE;

// Incluimos permisos especiales para el tema. Que pueden sobrescribir los por defecto del gestor.
	
	@include(APPPATH.'config'.DIRECTORY_SEPARATOR.'theme_permissions.php');