<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Control_library {

    public function session_check()
    {
    	$CI =& get_instance();

        $login_status = $CI->session->userdata('logged_in');

        if ($login_status == TRUE) 
        {
            $token = $CI->session->userdata('token');
            $user_id = $CI->session->userdata('id_user');
            $user_email = $CI->session->userdata('user_email');

            $valid_token = generate_session_token($user_id, $user_email);
            $integrity = ($token == $valid_token) ? TRUE : FALSE;

            if ($integrity === TRUE)
            {
                return $user_id;
            }
            else
                return FALSE;
        }

        $return_url = uri_string();

        if($CI->input->is_ajax_request())
        {
            header('HTTP', TRUE, 410);
            exit();            
        }
        else
        {
            redirect(base_url().GESTORP_MANAGER);
        }        
    }

}

/* End of file user_library.php */