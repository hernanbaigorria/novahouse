jQuery(document).ready(function() 
{ 
    page_components.init_drag();
    init_editables();
});

// Cargar el modal de configuración de un bloque.
$(document).on('click', '.btn-edit-component', function(e)
{ 
    var element             = $(this);
    var component           = $(this).parent('.page_component_header').parent('.page_component').data();
    var element_original    = element.html();
    var params = {
                    id: component.id,
                    type: component.type,
                    lang: component.lang
    };

    element.html('...');
    send_button('/Ajax/Ajax_editor/get_modal_component_editor/', params, function(data)
    {
        if (data.cod == 1) 
        {
            $('#modal-edit-component').remove();
            $(".page_components_containers").append(data.ext.composer);
            $('#modal-edit-component').modal({backdrop: 'static', keyboard: true});
            $('.only-on-modal').removeClass('hidden');
            element.html(element_original);
            start_masks();
            init_page_collection_sort();
            init_media_collection_sort();
        };
    });
    e.preventDefault();
});

$(document).on('submit', '.frm_save_configurations', function(e)
{ 
    $('#modal-edit-component').modal("toggle");
    // Actualizamos el preview.

    setTimeout(function() {
        location.reload();
    }, 500);
});

// Cargar elimina un bloque
$(document).on('click', '.btn-remove-component', function(e)
{ 
    var element             = $(this);
    var element_data        = $(this).data();
    var element_parent      = $(this).parent('.page_component_header').parent('.page_component');
    var component           = $(this).parent('.page_component_header').parent('.page_component').data();
    var element_original    = element.html();
    var params = {
                    id: component.id,
                    type: component.type,
                    lang: component.lang
    };

    bootbox.confirm(element_data.confirmation, function(result) 
    {
        if (result == true)
        {
            send_button('/Ajax/Ajax_editor/remove_block/', params, function(data)
            {
                if (data.cod == 1) 
                {
                    element_parent.remove();
                };
            });
        }
    });
    e.preventDefault();
});

var page_components = function () {

    return {
        //main function to initiate the module
        init_drag: function () {

            if (!jQuery().sortable) {
                console.log('GestorP: sortable lib is not loaded.')
                return;
            }

            $(".page_components_containers").sortable({
                connectWith: ".page_component",
                items: ".page_component", 
                opacity: 0.8,
                handle : '.component-dragger', //'.portlet-title',
                coneHelperSize: true,
                placeholder: 'page_components_placeholder',
                forcePlaceholderSize: true,
                tolerance: "pointer",
                helper: "clone",
                tolerance: "pointer",
                forcePlaceholderSize: !0,
                helper: "clone",
                revert: 250, // animation in milliseconds
                update: function(b, c) {
                    if (c.item.prev().hasClass("portlet-sortable-empty")) {
                        c.item.prev().before(c.item);
                    }
                                  
                    var order = [];
                    $('.page_component').each(function(i) { 
                       order.push($(this).data('id'));
                    });
                    
                    var params = {order: order.join(',')}
                    send_button('/Ajax/Ajax_editor/save_blocks_order/', params, function(data)
                    {
                        
                    });
                }
            });
        }
    };
}();

function init_editables()
{
    $('.gtp_editable_text').editable('/Ajax/Ajax_editor/save_content',
    {   
        loaddata    : { id_component:  $(this).data('id_component') },
        type        : 'textarea',
        tooltip     : 'Click to edit...',
        cssclass    : 'gtp_editable_input',
        submit      : 'Save',   
        width       : '100%',
        style       : 'inherit', 
        onblur      : 'ignore',  
     });

    $('.gtp_editable_html').editable('/Ajax/Ajax_editor/save_content',
    {   
        loaddata    : { id_component:  $(this).data('id_component') },
        type        : 'wysiwyg',
        tooltip     : 'Click to edit...',
        cssclass    : 'gtp_editable_input',
        submit      : 'OK',   
        style       : 'inherit',  
        width       : '100%',
        height      : 'auto',
        onblur      : 'ignore',
        submit      : 'Save',
        cancel      : 'Cancel',
        wysiwyg   : { toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                        ['fontname', ['fontname']],
                        ['fontsize', ['fontsize']], // Still buggy
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture', 'video', 'hr']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                      ],
                      onCreateLink : function(originalLink) {
                          return originalLink; // return original link 
                      }
                    }
    });
}