$(document).on('submit', '#frm_update_media', function(e) {
    send_complex_form(this, '/Ajax/Ajax_media/update_media', function(data) {
        if (data.cod == 1) {};
    });
    e.preventDefault();
});

$(document).on('click', '.btn-delete-media', function(e) {
    var params = $(this).data();

    bootbox.confirm(params.confirmation, function(result) {
        if (result == true) {
            send_button('/Ajax/Ajax_media/remove_media/', params, function(data) {
                if (data.cod == 1) {

                };
            });
        }
    });

    e.preventDefault();
});

$(document).on('submit', '#frm_replace_media', function(e) {
    send_complex_form(this, '/Ajax/Ajax_media/replace_media', function(data) {
        if (data.cod == 1) {};
    });
    e.preventDefault();
});


// Genera los previews de una coleccion de imagenes.
function reload_media_previews(source) {
    var preview_items = $(source).val();
    var source = source;

    var params = {
        source: preview_items
    }

    send_button('/Ajax/Ajax_media/get_previews/', params, function(data) {
        if (data.cod == 1) {
            $(".media_collection_preview[data-source='" + source + "']").html(data.ext.preview_data);
            init_media_collection_sort();
        };
    });
}

// Genera un modal selector de items en la galeria.
$(document).on('click', '.btn-pick-media', function(e) {
    var element = $(this);
    var cfg_element = $(this).data('cfg');
    var max_items = $(this).data('max');
    var filter = $(this).data('filter');

    var element_original = element.html();
    var params = $(this).data();

    // Agregamos los elementos ya seleccionados.
    var seleccionados = $(this).parent('span').parent('.input-group').find('input').val();
    params.selecteds = seleccionados;

    element.html('<i class="fa fa-spinner fa-spin"></i>');
    send_button('/Ajax/Ajax_media/get_media_picker/', params, function(data) {
        if (data.cod == 1) {
            $('#modal-media-picker').remove();
            $("body").append(data.ext.composer);
            $('#modal-media-picker').modal({
                backdrop: 'static',
                keyboard: true
            });
            element.html(element_original);
            FormFileUpload.init();
        };
    });
    e.preventDefault();
});

// Galeria Uploader
var FormFileUpload = function() {
    return {
        init: function() {

            $('#fileupload').fileupload({
                disableImageResize: 	false,
                autoUpload: 			false,
                disableImageResize: 	/Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                loadImageMaxFileSize:   10485760, // 10 mb para imagenes
                maxFileSize: 	        10485760, // 10 mb para archivos generales.
                loadVideoMaxFileSize: 	262144000, // 250mb para viideos
                previewMaxWidth: 		40,
                previewMaxHeight: 		40,
                acceptFileTypes: 		/(\.|\/)(gif|jpe?g|png|svg|mp4|pdf|doc|docx|xls|xlsx|ppt|pptx)$/i, // Will be checked on server side to.
                stop: function() {
                	// Recargamos la galeria al terminar la carga.
                    reload_gallery()
                }
            });

            // Agregamos un handler para cuando termina la carga
            $('#fileupload').bind('fileuploadstop', function(e, data) {
                $("#btn-start-upload").addClass('hidden');
            });

            $('#fileupload').bind('fileuploadadd', function(e, data) {
                $("#btn-start-upload").removeClass('hidden');
            });
        }
    };
}();


// Obtiene mas items para la galeria.
$(document).on('click', '.btn_load_more_media_items', function(e) {
    var params 	= {};
    var prev 	= $('.btn_load_more_media_items').html();
    $('.btn_load_more_media_items').html('<i class="fa fa-spin fa-spinner"></i>');
    $('.btn_load_more_media_items').attr('disabled', false);

    params.pickable 	= $(".media-gallery").data('pickable');
    params.filter 		= $(".media-gallery").data('filter');
    params.offset 		= $(".media-gallery .media-item").length;
    params.selecteds 	= $(".media-gallery").data('selected');
    params.name_filter 	= $("#media_gallery_name_filter").val();
    params.group_filter = $("#media_gallery_group_filter").val();

	console.log('append');
    send_button('/Ajax/Ajax_media/get_more_media', params, function(data) {
        if (data.cod == 1) {
            $(".media-gallery").append(data.ext.feed);
        };
        $('.btn_load_more_media_items').html(prev);
        $('.btn_load_more_media_items').attr('disabled', false);
    });
});

// Reinicializa la galeria.
function reload_gallery() {
	console.log('reloaded');
    var params = $(".media-gallery").data();
    params.offset 			= 0;
    params.selecteds 		= $(".media-gallery").data('selected');
    params.name_filter 		= $("#media_gallery_name_filter").val();
    params.group_filter 	= $("#media_gallery_group_filter").val();

    // Limpia todos los items. Carga desde cero.
    jQuery(".media-gallery .media-item").remove();

    send_button('/Ajax/Ajax_media/get_more_media', params, function(data) {
        $(".media-gallery").append(data.ext.feed);
        
        setTimeout(function() {
            Metronic.initSlimScroll('.scroller');
        }, 500);

        $(".template-upload.fade").remove();
    });
}

// Marca como seleccionado un item en el modal de galeria.
$(document).on('click', '.media-item.pickable', function(e) {
    var item = $(this).data();

    if ($(this).hasClass('picked')) {
        // Le quitamos el indicador visual
        $(this).removeClass('picked');

        // Lo removemos de la lista de pickeds.
        remove_from_selecteds(item.media);
    } else {
        var current_count = $('.media-item.pickable.picked').length;
        var max_count = $('#max_items').val();

        if (max_count == 0) {
            // Le agregamos el indicador visual
            $(this).addClass('picked');

            // Lo agregamos a la lista de pickeds
            add_to_selecteds(item.media);
        } else {
            if (current_count < max_count) {
                // Le agregamos el indicador visual
                $(this).addClass('picked');

                // Lo agregamos a la lista de pickeds
                add_to_selecteds(item.media);
            } else {
                // Identificamos el ultimo para desmarcarlo
                var a_quitar = $('.media-item.pickable.picked').last();
                // Lo quitamos de la lista de pickeds.
                remove_from_selecteds(a_quitar.data('media'));

                // Le quitamos el indicador visual.
                a_quitar.removeClass('picked');

                // Le agregamos el indicador visual al nuevo.
                $(this).addClass('picked');

                // Lo agregamos a la lista de pickeds al nuevo.
                add_to_selecteds(item.media);
            }
        }
    }
});

function add_to_selecteds(item_id) {
    var current_list = $(".media-gallery").data('selected').toString().split(',');
    if (item_id.length != null)
        current_list.push(item_id);
    $(".media-gallery").data('selected', current_list.join(','));
}

function remove_from_selecteds(item_id) {
    var current_list = $(".media-gallery").data('selected').toString().split(',');
    var index = current_list.indexOf(item_id.toString());
    if (index != -1) {
        current_list.splice(index, 1);
    }
    $(".media-gallery").data('selected', current_list.join(','));
}

// Lleva la seleccion del modal al item de configuracion.
$(document).on('submit', '#frm-media-picker', function(e) {
    var cfg_item = $('#frm-media-picker input[name="cfg_item"]').val();
    var cfg_item_id = "#" + cfg_item;
    var items = [];

    // Listamos los items a mostrar.
    $(".media-item.pickable.picked").each(function(index) {
        var media_item = $(this).data('media');
        items.push(media_item)
    });

    // Colocamos los ids de las imagenes en el input.
    $(cfg_item_id).val(items.join());

    // Actualizamos los previews, mostrando thumbs de las imagenes con id.
    reload_media_previews(cfg_item_id);

    // Cerramos el modal
    $('#modal-media-picker').modal("toggle");

    e.preventDefault();
});

// Para filtrar los resultados de la galeria.
$(document).on('keyup', '#media_gallery_name_filter, #media_gallery_group_filter', function(e) {
    typing_delay(function() {
        reload_gallery();
    }, 300);

});

// Para la gestion de grupos

	// Marca como seleccionado un item en el modal de galeria.
	$(document).on('click', '.btn-new-media-group', function(e) {
	    $("#modal-create-group").modal('toggle');
	});

	$(document).on('submit', '#frm-create-media-group', function(e) {
	    send_complex_form(this, '/Ajax/Ajax_media/create_group', function(data) {
	        if (data.cod == 1) {};
	    });
	    e.preventDefault();
	});

	$(document).on('click', '.btn-remove-media-group', function(e) {
	    var params = $(this).data();

	    bootbox.confirm(params.confirmation, function(result) {
	        if (result == true) {
	            send_button('/Ajax/Ajax_media/remove_media_group/', params, function(data) {
	                if (data.cod == 1) {

	                };
	            });
	        }
	    });

	    e.preventDefault();
	});

	$(document).on('click', '.btn-edit-group', function(e) {
	    $("#modal-edit-group").modal('toggle');
	    $('#txt-group-id').val($(this).data('id_group'));
	    $('#txt-group-name').val($(this).data('group_name'));
	});

	$(document).on('submit', '#frm-update-media-group', function(e) {
	    send_complex_form(this, '/Ajax/Ajax_media/update_group', function(data) {
	        if (data.cod == 1) {};
	    });
	    e.preventDefault();
	});