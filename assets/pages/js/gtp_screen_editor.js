$(document).ready(function() 
{ 
    refresh_vw_options();
});

$(window).resize(function() 
{ 
    refresh_vw_options();
});

// Cargar el modal de cambio de foto de perfil.
$(document).on('click', '.btn-config-page', function(e)
{ 
    var element             = $(this);
    var component           = $(this).parent().data();
    var element_original    = element.html();
    var params              = $('#page_data').data();

    element.html('...');
    send_button('/Ajax/Ajax_editor/get_modal_page_configuration/', params, function(data)
    {
        if (data.cod == 1) 
        {
            $('#modal-config-page').remove();
            $(".page-content").append(data.ext.composer);
            $('#modal-config-page').modal("toggle");
            $('.only-on-modal').removeClass('hidden');
            element.html(element_original);
            start_masks();
            init_page_collection_sort();
            init_media_collection_sort();
        };
    });
    e.preventDefault();
});

$(document).on('submit', '.frm_save_configurations', function(e)
{ 
    $('#modal-config-page').modal("toggle");

    setTimeout(function() {
        $( '#ifr-editor' ).attr( 'src', function ( i, val ) { return val; });
    }, 500);
});

$(document).on('click', '.btn-add-block', function(e)
{ 
    var element             = $(this);
    var element_original    = element.html();
    var params              = $('#page_data').data();
        params.th_component = $(this).data('component');

    send_button('/Ajax/Ajax_editor/add_block/', params, function(data)
    {
        if (data.cod == 1) 
        {
            $( '#ifr-editor' ).attr( 'src', function ( i, val ) { return val; }); 
            setTimeout(function() {
                var $contents = $( '#ifr-editor' ).contents();
                $contents.scrollTop($contents.height());
            }, 500);
        };
    });
    e.preventDefault();
});

function refresh_vw_options()
{
    var max_width = parseInt($('.vwp-container').css('width'));
    $('.btn-switch-vw-size').each(function() {
      var this_size = $(this).data('size');
      var this_elem = $(this);

      if (this_size < max_width)
        this_elem.removeClass('hidden');
      else
        this_elem.addClass('hidden');
    });
}