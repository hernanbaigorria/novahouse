function send_form(form, url, callback)
{
  $.ajax({
    type: "POST",
    url: url,
    data: form.serialize(),
    success: function(data, textStatus, jqXHR)
    {
        procesar_respuesta(data);
        
        if(typeof data !== "undefined"){
          if (typeof callback !== "undefined") {
            callback(data);                      
          };
        }                  
    },
    error: function(jqXHR, textStatus, errorThrown) 
    {
        procesar_errores_ajax(jqXHR);
    },
    dataType: "json"
  });
}

function send_button(url, params, callback)
{
  $.ajax({
    type: "POST",
    url: url,
    data: params,
    success: function(data, textStatus, jqXHR)
    {
        procesar_respuesta(data);
        
        if(typeof data !== "undefined"){
          if (typeof callback !== "undefined") {
            callback(data);                      
          };
        }                  
    },
    error: function(jqXHR, textStatus, errorThrown) 
    {
        procesar_errores_ajax(jqXHR);
    },
    dataType: "json"
  });
}

function load_url(url, params, div, callback)
{
  $.post( url, params, function( data ) {
    $( div ).html( data );
    callback();
  });
}

function procesar_respuesta(data)
{
  if (data !== null) 
  {
    if (data.display == 1)
    {
      if (data.tipo == 'success')
        { var duracion = 1000; }
      else
        { var duracion = 4000; }

      //MostrarNotificacion(data.tipo, data.mensaje);
      $.bootstrapGrowl(data.mensaje, {
          ele: 'body', // which element to append to
          type: data.tipo, // (null, 'info', 'danger', 'success', 'warning')
          offset: {
              from: 'top',
              amount: parseInt(100)
          }, // 'top', or 'bottom'
          align: 'center', // ('left', 'right', or 'center')
          width: 'auto', // (integer, or 'auto')
          delay: duracion, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
          allow_dismiss: true, // If true then will display a cross to close the popup.
          stackup_spacing: 10 // spacing between consecutively stacked growls.
      });
    }

    if (data.ext)
    {
      if (data.ext.action == 'redirect') 
      {
        setTimeout(function(){
          window.location = data.ext.redirect_target;
        }, 
        data.ext.redirect_delay);
      };

      if (data.ext.action == 'share') 
      {
        share_post(data.ext.share_type, data.ext.share_id, data.ext.share_why);
      };
      
      if (data.ext.action == 'reload') 
      {
        setTimeout(function(){
          location.reload();
        }, 
        data.ext.reload_delay);
      };

    }

  };
}
function procesar_errores_ajax(jqXHR)
{
    switch (jqXHR.status) {
        case 500: alert('Ocurrio un error con la ultima accion. Intente nuevamente.'); break;
        case 410: window.location.replace("/manager"); break;
    }                  
}

function send_complex_form(form, url, callback)
{
  var formURL = url;
  var elform = form;

  if(window.FormData !== undefined)
  {
      var formData = new FormData(elform);
      $.ajax({
          url: formURL,
          type: 'POST',
          data:  formData,
          mimeType:"multipart/form-data",
          contentType: false,
          cache: false,
          processData:false,
          dataType: 'json',
          success: function(data, textStatus, jqXHR)
          {
            procesar_respuesta(data);
            callback(data);                      
          }        
     });
  }
  else
  {
      //generate a random id
      var  iframeId = 'unique' + (new Date().getTime());   
      //create an empty iframe
      var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');   
      //hide it
      iframe.hide();   
      //set form target to iframe
      formObj.attr('target',iframeId);   
      //Add iframe to body
      iframe.appendTo('body');
      iframe.load(function(e)
      {
          var doc = getDoc(iframe[0]);
          var docRoot = doc.body ? doc.body : doc.documentElement;
          var data = docRoot.innerHTML;              
      });   
  } 
}
